<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html lang="pt-br">


<head>
	<meta charset="utf-8" >
	<meta name="viewport" content="initial-scale=1">
	
	<link rel="icon" href="<c:url value="/resources/imagens/stefanini-logo.png" /> ">
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/login.css" />">
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/config.js" />"></script>

	<title> Login </title>

</head>
<body>
	<c:import url="/redefinirsenha.jsp" />
<div id="branco"></div>
<div id="login">

	<span id="linha"></span>
	
	<input class="campoLogin" type="text" name="" id="usuario" placeholder="USUARIO"  required="required"/>
	<input class="campoLogin" type="password" name="" id="senha" placeholder="SENHA" required="required"/>

	<span id="barra"></span>
	 <button type="button" id="entrar" onclick="logar()">ENTRAR</button> 

	 <div id="loginInvalido">
	 	<p>Usuario e senha inválidos</p>
	 </div>

	<a id="esqueciSenha" href="#" >ESQUECI A SENHA</a>

</div>
	<footer> Desenvolvimento | 2017</footer>


<script>
function logar(){
	
	var usuarioObj = {
		"email": $("#usuario").val(),
		"senha": $("#senha").val()
	}
	
	$.ajax({
		url: localIP + "/baseDeConhecimento/login",
		//url: "https://private-310fa-stefaninierror.apiary-mock.com/login",
		dataType: "json",
		method: "POST",
		contentType: "application/json",
		data: JSON.stringify ( usuarioObj ),
	}).done(function(response, textstatus, teste) {
		console.log(teste.status);
		console.log(response);  
		obj_token = response.token;
		obj_usuario = response.usuario;
		localStorage.clear();
		localStorage.setItem("token", obj_token.token);
		localStorage.setItem("nome_usuario", obj_usuario.nome);
		localStorage.setItem("usuario_tipo", obj_usuario.tipoUsuario);
		localStorage.setItem("usuario_id", obj_usuario.id);
		
		$.each(response.usuario.empresa, function(index){
			console.log(index)
			localStorage.setItem("empresa"+(index), response.usuario.empresa[index].id);	
		});
		
		/*Pega a posicao do elemento na tela*/
		var posicao = $("#entrar").offset().left;

		 $('#branco').css("animation", "aa 0.8s")
         $('label').fadeOut(300);
         $('#linha').fadeOut(300);
         $('#usuario').fadeOut(1000);
         $('#senha').fadeOut(1000);
         
         $('#entrar').css({"left": -posicao,  "transition": "all .5s"});
         
        setTimeout(carregarBarra, 500);
        
	}).fail(function(response, textstatus, teste) {
		console.log(teste.status);
		console.log(response.status);
		console.log("CODIGO = " + textstatus);
		console.log("codigo fail");
		
		$('#loginInvalido').addClass('erro');
    	setTimeout(function() {
       		$('#loginInvalido').removeClass('erro');
    	}, 4000);
		
	});
}


$( "#usuario" ).focus(function() {
  $( this ).css("background-image", "url(resources/imagens/usuario2.png)");
});

$( "#senha" ).focus(function() {
  $( this ).css("background-image", "url(resources/imagens/senha2.png)");
});


function carregarBarra(){
	$("#entrar").css("opacity", "0");
	$('#barra').css({"opacity": "1", "height": "100%", "top": "0", "transition": "all .5s"});
	setTimeout(carregarProximaPagina, 500)
}

function carregarProximaPagina(){
	$(window.location.href = "view/analista/analista.jsp").fadeIn('500');
}

$(document).ready(function(){
	$("#usuario").focus();
});

$("#usuario").keypress(function(e) {
    if(e.which == 13) {
        $("#senha").focus();
    }
});
$("#senha").keypress(function(e) {
    if(e.which == 13) {
        $("#entrar").trigger("click");
    }
});

$("#esqueciSenha").click(function() {
	$("#login").fadeOut('500');
	
	/*Campos redefinir senha*/
	$("#email-usuario").val("");
	$("#resposta-seguranca").val("");
	$("#resposta-seguranca").css("display", "none");
	
	$("#email-usuario").attr("disabled", false)
	
	$("#nova-senha").val("");
	$("#nova-senha").css("display", "none");
	
	$("#perguntaSeguranca").text(" ");
	
	$("#container-redefinir-senha").css({"height": "200px","margin-top": "-100px"});
	$("#proximo").css("display", "flex")
	$("#confirmar").css("display", "none");
	$("#confirmar").css({"box-shadow": "inset 0px -100px 5px #000", "border-top": "2px solid #fff"})
	
	
	$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
	$("#container-redefinir-senha").fadeIn('500');
	$("#container-redefinir-senha").css({"opacity": "1", "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})
	$("#proximo").css('z-index', '1')
	$("#confirmar").css('z-index', '0')
});

$("#total").click(function(){
	$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
	$("#container-redefinir-senha").css({"opacity": "0", "animation": "none", "z-index": "-10"});
	$("#login").fadeIn('500');
})


</script>
</body>
</html>