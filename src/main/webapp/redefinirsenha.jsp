<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet"/>
<link type="text/css" href="<c:url value="/resources/css/redefinirsenha.css" />" rel="stylesheet"/>
<script type="text/javascript" src="<c:url value="/resources/js/config.js" />"></script>



<section id="container-redefinir-senha">
		<form id="formulario" enctype="multipart/form-data">
			<div id="titulo">
				<h1>Redefinir Senha</h1>
			</div>
			<ul>
				<li>
					<label>Email</label>
					<input type="text" id="email-usuario">
				</li>
				
				<li>
					<label class="label-seguranca">Pergunta de Segurança</label>
					<p id="perguntaSeguranca"></p>
				</li>
				
				<li>
					<label class="label-seguranca">Resposta de Segurança</label>
					<input type="text" id="resposta-seguranca">
				</li>
				
				<li>
					<label class="label-seguranca">Nova senha</label>
					<input type="password" id="nova-senha"><img id="imagem-senha" src="resources/imagens/eye.png">
				</li>
			</ul>	
			
			<button id="proximo" type="button" onclick="validarEmail()" ><span id="textobotao">Próximo</span></button>
			<button id="confirmar" type="button" onclick="confirmarUsuario()" ><span id="textobotao">Confirmar</span></button> 	
		</form>
</section>
<div id="total"></div>

<script type="text/javascript">
	var formData = '';
	var usuarioObj = '';

	function validarEmail() {
		if($("#email-usuario").val() == ''){
			$("#email-usuario").attr({'placeholder': 'campo email não pode estar vazio', 'value': ''});		
			$("#email-usuario").addClass("erroEmail");
		}else{
			$("#email-usuario").removeClass("erroEmail");
			buscarUsuario();
		}
	}
	
	function buscarUsuario() {
		formData = new FormData();
		
		formData.append('email', $("#email-usuario").val());
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario/verificarEmail",
			method: "POST",
			data : formData,
			processData : false,
			contentType : false,
			cache : false,
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
			}).done(function( response){
				aviso(response);
								
			}).fail(function(response, textstatus, teste){
				console.log("DEU FAIL")
				console.log(teste.statu);
				console.log(response.status);
				console.log(textstatus);
			});	
	}
	
	
	function aviso(objUsuario) {
		
		
		console.log(objUsuario)
		
		if (objUsuario == '') {
			$("#email-usuario").val('digite um email válido');		
			$("#email-usuario").addClass("erroEmail");
		}else{			
			if(objUsuario.perguntaSeguranca == null){
				$("#perguntaSeguranca").html("Sua pergunta de segurança não foi preenchida!<br /> Entre em contato com administrador.");
				$("#perguntaSeguranca").css({"color": "#FF0000"})
				$("#container-redefinir-senha").css({"height": "320px","margin-top": "-160px"});
			}else{
				$("#proximo").css('z-index', '0')
				$("#confirmar").css('z-index', '1')
				var perguntaSeguranca = objUsuario.perguntaSeguranca;
				
				$("#perguntaSeguranca").html(perguntaSeguranca);
				$("#perguntaSeguranca").css({"color": "#73BFCB"})
				
				$(".label-seguranca").css({"display": "flex"});
				
				$("#resposta-seguranca").css({"display": "flex"});
				
				$("#nova-senha").css({"display": "flex"});
				
				$("#container-redefinir-senha").css({"height": "500px","margin-top": "-250px"});
				
				$("#email-usuario").attr('disabled', true);
				
				$("#imagem-senha").css({"display": "flex"});
				
				$("#confirmar").css({"display": "flex"});
				
				$("#proximo").css({"display": "none"});
				
				localStorage.setItem("usuario_id", objUsuario.id);
				localStorage.setItem("usuario_nome", objUsuario.nome);
				localStorage.setItem("usuario_email", objUsuario.email);
				localStorage.setItem("usuario_tipo", objUsuario.tipoUsuario);
				localStorage.setItem("pergunta_seguranca", objUsuario.perguntaSeguranca);
			}
		}
	}
	
	function verificarTipoDeErro(erro){
		if(erro.status == 401){
			console.log("IIIIIIIIIIIIIIIIIIIIIIIH MONAMOUR")
			$("#resposta-seguranca").val("");
			$("#resposta-seguranca").attr({'placeholder': 'Resposta inválida', 'value': ' '});		

		} else {
			console.log("NADA ACONTECEU MONAMOUR")
		}
	}	
		
	function confirmarUsuario() {
		formData = new FormData();
		
		formData.append('id', localStorage.getItem("usuario_id"));
		formData.append('resposta', $("#resposta-seguranca").val());
		formData.append('senha', $("#nova-senha").val());

		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario/redefinirSenha",
			method: "POST",
			data : formData,
			processData : false,
			contentType : false,
			cache : false,
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
			}).done(function(response, textstatus, teste){
				console.log("ENTROU NO DONE")
				console.log(response);
				
				localStorage.clear();	
				
				$("#confirmar").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
			}).fail(function(response, textstatus, teste){
				verificarTipoDeErro(response);
				
				console.log("DEU FAIL")
				console.log(teste.statu);
				console.log(response.status);
				console.log(textstatus);
			});	
	}
	
	$("#imagem-senha")
	  .mouseup(function() {
		  $("#nova-senha").attr('type', 'password');
	  })
	  .mousedown(function() {
		  $("#nova-senha").attr('type', 'text');
	  });
	
	 	
</script>
