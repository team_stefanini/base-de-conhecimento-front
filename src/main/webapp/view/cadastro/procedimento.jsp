<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>

<link type="text/css"  href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css"  href="<c:url value="/resources/css/cadastroprocedimento.css" />" rel="stylesheet" />

<title>Stefanini</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script type="text/javascript" src="<c:url value="/resources/js/config.js" />"></script>
<script  src="<c:url value="/resources/ckeditor/ckeditor.js" />"></script> 
<%-- <script  src="<c:url value="/resources/tinymce/js/tinymce/tinymce.min.js" />"></script> 
<script>tinymce.init({ selector:'#container-descricao #descricao', language: 'pt_BR' });</script> --%>


</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	
	<div id="modal-confirmar">
		<h2>Cadastro</h2>
					
		<span id="mensagemDeConfirmacao"> </span>
					
		<div id="mensagemDeConfirmacaoBotoes">
			<input type="button" id="btnSim" value="OK" onclick="confirmarCadastro()">
		</div>
	</div>
	
	<div id="totalModal"></div>
	
	<div id="container">
		<div id="container-cadastro-procedimento">
				<form id="formulario" enctype="multipart/form-data">
					<input type="hidden" id="idProcedimento">
					<div id="titulo">
						<h1>Procedimento</h1>
					</div>			
						<div id="um">
						<input class="erro-input procedimento-campos-cadastro" type="text" id="tituloProcedimento" name="titulo" autofocus="autofocus" placeholder="Titulo" >
						
						<input class="erro-input procedimento-campos-cadastro" list="empresas" id="empresa" placeholder="Empresa"> 
						<datalist id="empresas"></datalist>
						
						<select  class="procedimento-campos-cadastro"  id="procedimento-aprovacao" >
							<option value='' disabled="disabled" selected="selected">Aprovação</option>
							<option value="false">Sim</option>
							<option value="true">Não</option>
						</select>
						
						</div>
					<div id="dois">
					<input class="erro-input procedimento-campos-cadastro"  type="text" id="categoria" list="categorias" placeholder="Categoria"> 
					<datalist class="campos" id="categorias"></datalist> 
				
					<input type="text" list="niveis-1" class="niveis erro-input procedimento-campos-cadastro" id="nivel1" placeholder="nivel 1" > 
					<datalist id="niveis-1"></datalist> 
							
					<input type="text" list="niveis-2" class="niveis erro-input procedimento-campos-cadastro" id="nivel2" placeholder="nivel 2">  
					<datalist id="niveis-2"></datalist>
								 
					<input  type="text" list="niveis-3" class="niveis erro-input procedimento-campos-cadastro" id="nivel3" placeholder="nivel 3"> 
					<datalist id="niveis-3"></datalist>
					
					<input type="text" list="niveis-4" class="niveis erro-input procedimento-campos-cadastro" id="nivel4" placeholder="nivel 4"> 
					<datalist id="niveis-4"></datalist>
					</div>
					
					<div id="tres">
					<select class="select-vencimento-elegivel procedimento-campos-cadastro" id="vencimento" >
							<option value='' disabled="disabled" selected="selected">Vencimento</option>
							<option value="6">6 Meses</option>
							<option value="12">12 Meses</option>
						</select>	
							
						<select class="select-vencimento-elegivel procedimento-campos-cadastro" id="elegivel">
							<option value='' disabled="disabled" selected="selected">Elegivel</option>
							<option value="true">Sim</option>
							<option value="false">Não</option>
						</select>							
							
						<input class="erro-input procedimento-campos-cadastro" id="grupoSolucao" list="gruposdesolucao" placeholder="Grupo Solucao"> 
						<datalist id="gruposdesolucao"></datalist> 
					</div>
					<div id="container-descricao">
						<textarea id="descricao" rows="10" cols="50" ></textarea>
					</div>
					
					
					<div id="container-imagens"> 
						<ul>
						</ul> 
					</div>
					
					<button type="button" onclick="criarProcedimento()" id="btncadastrar" > <span id="textobotao">Salvar </span>></button>		
				</form>
		</div>
	</div>
	
	<script>
	var editor = CKEDITOR.replace( 'descricao',{
		width: '100%',
		height: '300'
	} );
	
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	});
	
	/* Verificar se selects estão vazios */
	function verificarCampos(){
		$("#formulario  .erro-input").each(function(){
			if ($(this).val() == ''){ 
				$(this).addClass("erro");
				console.log("ENTROU")
			}else{
				$(this).removeClass("erro");
			}	
		});
	};
	
	/* checa se o usuario vai editar um procedimento */
	$(document).ready(function(){
			if (localStorage.getItem("editar_procedimento_id") != null )
					carregarInformacoesDoProcedimento(); 
	});
	
	/* mostra a modal para confirmar a criaçao do procedimento*/
	function confirmar(){
		$("#totalModal").css({"opacity": "1", "z-index": "10"});
		$("#modal-confirmar").css({"opacity": "1", "z-index": "11", "animation": "movimento .2s", "animation-fill-mode": "forwards"});
	}
	
	/* redireciona para a página inicial*/
	function confirmarCadastro(){
		$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
	};
	
	/*Fecha a modal se o usuario clicar fora dela*/
	$("#totalModal").click(function(){
		$("#totalModal").css({"opacity": '', "z-index": ''});
		$("#modal-confirmar").css({"opacity": '', "z-index": '', "animation": '', "animation-fill-mode": ''});
	});
	
	var description = '';
	function carregarInformacoesDoProcedimento(){
		$.ajax({
			url : localIP + "/baseDeConhecimento/procedimento/" + localStorage.getItem("editar_procedimento_id"),
			type : 'GET',	
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			$("#tituloProcedimento").val(response.titulo);
			description = response.descricao;
			carregarTodosDatalist(response);
		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
			$("#textobotao").text("Erro")
			$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

		});
	}
	
	 
 	function carregarTodosDatalist(response){
 		editor.setData(response.descricao)
		$("#container-cadastro-procedimento #formulario #um #empresa").val(response.categoria.empresa.nome); 

	 	empresaID = response.categoria.empresa.id;
			 	
	 	if (response.grupoSolucao != null) {
			idGrupoSolucao = response.grupoSolucao.id;
			$("#tres #grupoSolucao").val(response.grupoSolucao.nome)
	 	}else{
	 		$("#tres #grupoSolucao").val('');
	 	}
		categoriaID = response.categoria.id;
		
		$("#categoria ").val(response.categoria.categoria.categoria.categoria.categoria.nome);
		$("#nivel1 ").val(response.categoria.categoria.categoria.categoria.nome);
		$("#nivel2 ").val(response.categoria.categoria.categoria.nome);
		$("#nivel3 ").val(response.categoria.categoria.nome);
		$("#nivel4 ").val(response.categoria.nome); 

 	}
	
	
	/* Limpar os datalist quando o usuario trocar a opção*/
	$("#formulario #empresa").on('input', function(){
		$("#categorias").empty();
		$("#categoria").val("");
		$("#nivel1").empty();
		$("#nivel2").empty();
		$("#nivel3").empty();
		$("#nivel4").empty();
		console.log("limpei as categorias")
	});
	
	$("#categoria").on('input', function(){
		$("#nivel1").empty();
		$("#nivel2").empty();
		$("#nivel3").empty();
		$("#nivel4").empty();
		console.log("limpei os niveis")
	});
	
	$("#nivel1").on('input', function(){
		$("#nivel2").empty();
		$("#nivel3").empty();
		$("#nivel4").empty();
		console.log("limpei os niveis 3 e 2")
	});
	
	$("#nivel2").on('input', function(){
		$("#nivel3").empty();
		$("#nivel4").empty();
		console.log("limpei o nivel 3")
	});
	
	$("#nivel3").on('input', function(){
		$("#nivel4").empty();
		console.log("limpei o nivel 4")
	});
	
	/*  procedimento */
	
	var vencimento = '';
	var elegivel = '';
	var categoriaID = '';
	var vencimentoEscolhido = '';
	var dataEscolhida = '';
	var mesAtual = '';
	var vecimento = '';
	var dia = '';
	var mes = '';
	var ano = '';
	var dataCompleta = '';
	var usuarioObj = '';
	var categoriaObj = '';
	var grupoSolucaoObj = '';
	var procedimentoObj = '';
	var idGrupoSolucao = '';
	var empresaID = '';
	var status = '';
	
	function criarProcedimento(){
		 elegivel = $("#elegivel option:selected").val();
		 vencimentoEscolhido = $("#vencimento option:selected").val();
		 status= $("#procedimento-aprovacao option:selected").val();
		 
		console.log("VENCIMENTOOOO: " + vencimentoEscolhido );
		console.log("STATUS : " + status);
		
		/*Criar a data de vencimento*/
		
		/*Pega a data atual*/
		dataEscolhida = new Date();
			
		/*Pega o mês da data atual*/
		mesAtual = dataEscolhida.getMonth();
		
		/*Transforma a opção de meses escolhida de string para int*/
		vecimento = parseInt(vencimentoEscolhido)
		
		/*Soma o mes atual com a opção escolhida*/
		dataEscolhida.setMonth(mesAtual + vecimento);
		
		/*Pega o dia da data escolhida*/
		dia = dataEscolhida.getDate();
		
		/*Pega o mês da data escolhida*/
		/*Janeiro = 0 / Dezembro = 11*/
		mes = dataEscolhida.getMonth();
		mes += 1;
		
		/*Pega o ano da data escolhida*/
		ano = dataEscolhida.getFullYear();
		
		/*Transforma a data em formato string, juntando as partes*/
		dataCompleta = dia+"/"+mes+"/"+ano;
		
		console.log("CERTAAAAAAAA " + dataCompleta);
		
		usuarioObj= {
			"id" :  localStorage.getItem("usuario_id")
		};
		
		categoriaObj = {
				"id" : categoriaID
		};
			
		grupoSolucaoObj = { 
			"id" : idGrupoSolucao,
		}
		
		if (localStorage.getItem("editar_procedimento_id") == null) {
			cadastrarProcedimento();			
		}else{
			alterarProcedimento();
		}
	}	
	
	function cadastrarProcedimento(){
		
		if (grupoSolucaoObj.id == '' || grupoSolucaoObj.id == undefined ) {
			
			procedimentoObj = {
					"titulo": $("#tituloProcedimento").val().toUpperCase(),
					"descricao" : editor.getData(),
					"elegivel": elegivel,
					"vencimento": dataCompleta,
					"usuario": usuarioObj,
					"categoria": categoriaObj,
					"status" : status,
			}

		}else{
			procedimentoObj = {
					"titulo": $("#tituloProcedimento").val().toUpperCase(),
					"descricao" : editor.getData(),
					"elegivel": elegivel,
					"vencimento": dataCompleta,
					"usuario": usuarioObj,
					"categoria": categoriaObj,
					"grupoSolucao": grupoSolucaoObj,
					"status" : status,
			}
		}
		
		
	 	verificarCampos();

		$.ajax({
			url: localIP + "/baseDeConhecimento/procedimento",	
			dataType : "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify ( procedimentoObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste){
			$("#mensagemDeConfirmacao").text("Procedimento cadastrado com sucesso")
			confirmar();	
			console.log("DONE");
			console.log("RESPONSE = " + response);
			console.log("TEXTSTATUS = " + textstatus);
			console.log("TESTE = " + teste);
			
		}).fail(function(response, textstatus, teste){
			verificarTipoDeErro(response)
			
			console.log("FAIL");
			console.log("RESPONSE = " + response.status);
			console.log("TEXTSTATUS = " + textstatus);
			console.log("TESTE = " + teste);
			console.log("CATEGORIA  =  " + categoriaObj.id)
			
		});  
	}
	
	function verificarTipoDeErro(erro){
		if(erro.status == 600){
			$("#tituloProcedimento").val("");
			$("#tituloProcedimento").attr({'placeholder': 'Título já cadastrado', 'value': ' '});		
			$("#tituloProcedimento").addClass("erro");
			console.log("_____________________________________________ Procedimento igual")
		} else {
			if(erro.status == 400){
				$(".select-vencimento-elegivel").addClass("erro");
				
			}else{
				console.log("_________________________ Segue o jogo no beira rio!")	
			}
		}
	}
	
	function alterarProcedimento(){
		verificarCampos();
		
	if (grupoSolucaoObj.id == '' || grupoSolucaoObj.id == undefined ) {
			
			procedimentoObj = {
					"id": localStorage.getItem("editar_procedimento_id"),
					"titulo": $("#tituloProcedimento").val().toUpperCase(),
					"descricao" : editor.getData(),
					"elegivel": elegivel,
					"vencimento": dataCompleta,
					"usuario": usuarioObj,
					"categoria": categoriaObj,
					"status" : status,
			}

		}else{
			procedimentoObj = {
					"id": localStorage.getItem("editar_procedimento_id"),
					"titulo": $("#tituloProcedimento").val().toUpperCase(),
					"descricao" : editor.getData(),
					"elegivel": elegivel,
					"vencimento": dataCompleta,
					"usuario": usuarioObj,
					"categoria": categoriaObj,
					"grupoSolucao": grupoSolucaoObj,
					"status" : status,
			}
		}
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/procedimento",
			dataType: "json",
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify ( procedimentoObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
			}
		}).done(function(response, textstatus, teste){
			$("#mensagemDeConfirmacao").text("Procedimento alterado com sucesso")
			confirmar();
			localStorage.removeItem("editar_procedimento_id")	
			
		}).fail(function(response, textstatus, teste){
			verificarTipoDeErro(response)
			console.log("deu erro")
			
		});
	}
	
	
	/*Popular o datalist com as empresas do banco*/
	var options = '';
	
	/*Popular o datalist com os grupos de solucao do banco*/
	var grupo = '';
	
	$(document).ready(function(){
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$("#empresas").empty();
			$.each(response, function(index){
				
				options =  '<option>' + response[index].nome + '</option>';
				
				$("#empresas").append("<option value =" + response[index].nome.replace(/\s/g," ") +" data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/grupo",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){
				
				grupo =  '<option>' + response[index].nome + '</option>';
				
				$("#gruposdesolucao").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
	});
	
	$("#grupoSolucao").on('input', function(){
		var value =  $('#grupoSolucao').val();
		idGrupoSolucao = $('#gruposdesolucao [value="' + value + '"]').data('id');
		console.log(idGrupoSolucao)
		console.log(value)
	});
	
	/*Popular o datalist com as categorias da empresa escolhida*/
	$("#formulario #empresa").on('input', function(){
		
		var value =  $('#formulario #empresa').val()
		var empresaID = $('#empresas [value="' + value + '"]').data('id'); 
		console.log("aaaa")
		console.log("Entrei na categoria ");
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/empresa/" + empresaID ,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$.each(response, function(index){
				console.log("Listei as categorias")
				
				$("#categorias").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro na categoria");
		});
		
		carregarImagensDaEmpresa(empresaID);
	});
	
	/* Popular o datalist de nivel 1 com os niveis da categoria escolhida */
	$("#categoria").on('input', function(){
		var value =  $('#categoria').val()
		var categoriaID = $('#categorias [value="' + value + '"]').data('id'); 
		
		console.log("Entrei no nivel 1");
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + categoriaID,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$.each(response, function(index){
				console.log("Listei o nivel 1")
				
				$("#niveis-1").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 1");
		});
	});
	
	/* Popular o  datalist de nivel 2  */
	$("#nivel1").on('input', function(){
		console.log("Entrei no nivel 2")
		
		var value =  $('#nivel1').val()
		var nivel = $('#niveis-1 [value="' + value + '"]').data('id');
		categoriaID = nivel;
		
		console.log("Nivel ===== "  + nivel)
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$.each(response, function(index){
				console.log("Listei o nivel 2")
				$("#niveis-2").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 2")
		});
	});
	
	/* Popular o datalist de  nivel 3 */
	$("#nivel2").on('input', function(){
		
		var value =  $('#nivel2').val()
		var nivel = $('#niveis-2 [value="' + value + '"]').data('id');
		categoriaID = nivel;
		
		console.log("entrei no nivel 3 ");
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$.each(response, function(index){
				console.log("Listei o nivel 3")
				$("#niveis-3").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 3")
		});
	});
	
	/* Popular o datalist de  nivel 4 */
	$("#nivel3").on('input', function(){
		
		var value =  $('#nivel3').val();
		var nivel = $('#niveis-3 [value="' + value + '"]').data('id');
		categoriaID = nivel;
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$.each(response, function(index){
				console.log("Listei o nivel 3")
				
				$("#niveis-4").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 3")
		});
	});
	
	$("#nivel4").on('input', function(){			
			var value =  $('#nivel4').val();
			var nivel = $('#niveis-4 [value="' + value + '"]').data('id');
			categoriaID = nivel;
			
		});

	
	/**/
	elegivel = $('input[name=vencimento]:checked').val();

	vencimento = $('input[name=vencimento]:checked').val();	
	
	function carregarImagensDaEmpresa(empresa){
		$("#container-imagens ul").empty();
		
		var liHTML = '';
			$.ajax({
				url: localIP + "/baseDeConhecimento/empresa/" + empresa + "/imagem",
				dataType: "json",
				contentType: "application/json",
				type: "GET",
				headers: {
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response){
				$.each(response, function(index){
					console.log(response[index].caminho);
					$("#container-imagens ul").append("<li>"+ "<img class='pequeno' src=" + localIP + "/baseDeConhecimento" + response[index].caminho + ">" + "</li>")
				});
			}).fail(function(response, textstatus, teste){
				console.log("martins")
				console.log(response);
				console.log(textstatus);
			});
	}
	
	$('a').click(function(){
		localStorage.removeItem("editar_procedimento_id");
	});
	
	CKEDITOR.on( 'instanceReady', function( evt ) {
	    console.log("Editor esta pronto");
	    console.log(description)
	    editor.setData(description)
	} );
	
	</script>
</body>
</html>