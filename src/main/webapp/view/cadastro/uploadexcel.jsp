<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css" href="<c:url value="/resources/css/uploadexcel.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<section id="container-cadastro-upload" >
		<form id="formulario" enctype="multipart/form-data">
			<div id="container-cadastro-upload-titulo">
				<h1> Cadastrar Categorias Excel </h1>
			</div>
					
			<label>Arquivo excel: </label>			
			<label id="selecionarArquivo">Selecionar arquivo</label>
			<input type="file" id="inputFile">
			
			<button type="button"  onclick="verificarUpload()" id="upload" ><span id="textobotao">Confirmar</span> </button>
			 
		</form>
	</section>	
		<div id="total"></div>
	<div id="float-adicionar-upload"> 
	</div>
	
	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	var inputFile = "";
	var formData = "";
	
	function verificarUpload(){
		$("#formulario input").each(function (){
			if($(this).val() == ''){
				$(this).addClass("erro");
			}else{
				$(this).removeClass("erro");
				uploadCategorias();
			}
			
		});
	}
	
	function uploadCategorias() {
		formData = new FormData();
		inputFile = document.getElementById("inputFile");
		
		formData.append('file', inputFile.files[0]);
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/uploadFile",
			method: "POST",
			data: formData,
			processData: false,
			contentType: false,
			cache: false,
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			$("#textobotao").text("sucessso")
			$("#upload").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
			
		});
	}
	
	
	$('#selecionarArquivo').on('click', function() {
		  $('#inputFile').trigger('click');
	})
	
	$("#float-adicionar-upload").click(function(){
		$("#textobotao").text("confirmar")
		$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
		$("#container-cadastro-upload").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
		$("#upload").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"});
	});
	
	$("#total").click(function(){
		$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
		$("#container-cadastro-upload").css({"animation": "none", "z-index": "-10"});
	})
	</script>