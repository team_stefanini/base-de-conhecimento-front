<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>



<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/usuario.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<div id="container-formulario-cadastro-usuario">
		<form action="" id="formulario-cadastro-usuario">
			<div id="tituloCadastroUsuario">
				<h1>Informações</h1>
			</div>
			<input type="hidden" id="idUsuario">
			<label class="titulos">Nome</label>
			<input type="text" autofocus id="nome" class='talvez-erro'>
			</br>
			<label class="titulos">Usuario</label>
			<input type="email" id="email" class='talvez-erro'>
			</br>
			<label class="titulos" id="lblSenha">Senha</label>
			<input type="password" id="senha">
			
			<div id="container-radio-buttons" >
				<label class="titulos">Tipo</label>
					<select id="selectTipoUsuario">
						<option value="0">Administrador</option>	
						<option value="1">Coordenador</option>
						<option value="2" selected="selected">Analista</option>						
					</select>
			</div>
						
			<div id="container-quantidade-empresas" >
				<!-- <label class="titulos" id="empresaLabel"></label> -->
				
				<label class="titulos">Empresa</label>
				<input type="text" id="pesquisaempresas" >	
				
				<ul id="empresas-checkbox">	
					
				</ul>
			</div>
			
			
			<button id="btnEntrar" type="button" onclick="verificar()"><span id="textobotao">Confirmar</span> </button>
		</form>
		<a id="link-perfil-usuario" href="../visualizar/perfilusuario.jsp">Mais</a>
		<div  class="botoes-alterar-excluir" >
				<div id="alterar"  >
					
				</div>
				<div id="excluir" >
					
				</div>
				
				
			</div>
		
	</div>
	
	<div id="total"></div>
	<div id="float-adicionar-usuario"></div>

	<script type="text/javascript">
	
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	
	/*Verifica se é um cadastro ou alteracao de usuario*/
	function verificar(){
		if ($("#idUsuario").val() == '') {
			criarUsuario();
		}else{
			alterarUsuario();
		}
	}
	
		$('#pesquisaempresas').keyup(function() {
		  var $rows = $('#container-quantidade-empresas ul li');
		  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');

		  $rows.hide().filter(function() {
		    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
		    var matchesSearch = true;
		    $(val).each(function(index, value) {
		      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
		    });
		    return matchesSearch;
		  }).show();
		});
	
	
	var quantidadeEmpresa = 0;
	var tipoUsuario = 2;
	var options = '';
	var empresaObj =  null;
	
	
	$("#selectTipoUsuario").change(function(){
		
		tipoUsuario = ($("#selectTipoUsuario").val());
			
			if (tipoUsuario == 0) {
					$("#pesquisaempresas").attr("disabled", true);
	    			$("input:checkbox[class=empresas]").attr("disabled", true);
			}else{
				$('#pesquisaempresas').attr('disabled', false);
		    	$("#container-quantidade-empresas ul").css("display", "block");
			}
	});
	
	function verificarCamposCadastroUsuario(){
		
		$("#formulario-cadastro-usuario .talvez-erro").each(function(){
			
			if ($(this).val() == ''){
				$(this).addClass("erro");
				return 0;
			}else{
				$(this).removeClass("erro");
				return 1;
			}
		});
	}
	
	
	function verificarTipoDeErro(erro) {
		if(erro.status == 601){
			$("#pesquisaempresas").attr("placeholder", "Selecione as empresas")
			console.log("deu erro")
			console.log("=============================")
		}else{
			if(erro.status == 400){
				$("#formulario-cadastro-usuario .talvez-erro").each(function(){
					$(this).addClass("erro");
				});
			}else{
				$("#formulario-cadastro-usuario .talvez-erro").each(function(){
					$(this).removeClass("erro");
				});
			}
		}		
	}
	
	function criarUsuario(){
	 	if (tipoUsuario != 0 ) {
	 		
	 		empresaObj = [];

		    $("input:checkbox[class=empresas]:checked").each(function() {

		        var id = $(this).val();

		        item = {}
		        item ["id"] = id;

		        empresaObj.push(item);
		    });

		    console.log(JSON.stringify(empresaObj));
		}
		 
		var usuarioObj = {
				"nome": $("#nome").val(),
				"email": $("#email").val(),
				"senha": $("#senha").val(),
				"tipoUsuario": $("#selectTipoUsuario").val(),
				"empresa": empresaObj	
		}
		
		console.log(JSON.stringify(usuarioObj));
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario",
			dataType: "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify ( usuarioObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			
			$("#textobotao").text("Usuario cadastrado")
			$("#btnEntrar").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
			
			/* var li = document.createElement('li');
				$(li).addclas('classe');
				$(li).text('texto');
				
				$("#container-quantidade-empresas ul").append(li); */ 
				
				var tdNomeHTML = document.createElement('td');
				$(tdNomeHTML ).text(response.nome);
				
				var tdTipoHTML = document.createElement('td');
				$(tdTipoHTML).text(response.tipoUsuario);
				
				var tdEmpresasHTML = document.createElement('td');
				$.each(response.empresa, function(index){
					$(tdEmpresasHTML).append(response.empresa[index].nome + ". ");
				})
				
				var tdEmailHTML = document.createElement('td');
				$(tdEmailHTML).text(response.email);
				
				var trHTML = document.createElement('tr');
				$(trHTML).attr('data-id', response.id);
				$(trHTML).attr('onClick', 'buscarUsuario(this)');
				
				$(trHTML).append(tdNomeHTML);
				$(trHTML).append(tdTipoHTML);
				$(trHTML).append(tdEmpresasHTML);
				$(trHTML).append(tdEmailHTML);
							
			$('#tabelaUsuarios tbody').append(trHTML);
			
			
			console.log(JSON.stringify(response))
			console.log(response)
			
		}).fail(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response.status);
			console.log("CODIGO = " + textstatus);
			console.log
			verificarTipoDeErro(response);
			
		});
		
	}
	
	function alterarUsuario(){
		
	 	verificarCamposCadastroUsuario();
	 	
	 	if (verificarCamposCadastroUsuario == 0) {
			console.log("true")
		}else{
			console.log("false")
		}
		
	 	if (tipoUsuario != 0 ) {	
			
	 		empresaObj = [];

		    $("input:checkbox[class=empresas]:checked").each(function() {

		        var id = $(this).val();

		        item = {}
		        item ["id"] = id;

		        empresaObj.push(item);
		    });

		    console.log(JSON.stringify(empresaObj));
			
		}
		 
		var usuarioObj = {
				"id": $("#idUsuario").val(),
				"nome": $("#nome").val(),
				"email": $("#email").val(),
				"tipoUsuario": $("#selectTipoUsuario").val(),
				"empresa": empresaObj	
		}
		
		
		console.log(JSON.stringify(usuarioObj));
		
		/*Altera o usuario */
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario",
			dataType: "json",
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify ( usuarioObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
			$("#textobotao").text("Usuario alterado")
			$("#btnEntrar").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
			carregarTabelaDeUsuarios();
			
		}).fail(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			console.log("CODIGO = " + textstatus);
			
			$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
			$("#textobotao").text("Erro ao alterar usuario")
			$( "#btnEntrar" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });
			
		}); 
	}
	
	$(document).ready(function(){
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){

				options =  '<option>' + response[index].nome + '</option>';
				
				/* var li = document.createElement('li');
				$(li).addclas('classe');
				$(li).text('texto');
				
				$("#container-quantidade-empresas ul").append(li); */
				
				 $("#container-quantidade-empresas ul").append("<li class='liempresas'>" + "<input class='empresas' type='checkbox' value=" + response[index].id + ">" + response[index].nome + "</li>"); 

			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
			
		});
		
	}); 
	
	function excluirUsuario(){	
		
		if ($("#idUsuario").val() == '') {
			console.log('vazio')
		}else{
		
			$.ajax({
				url : localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("busca"),
				dataType : "json",
				method: "DELETE",
				contentType : "application/json",
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				$("#tabelaUsuarios tbody tr[data-id=" + $("#idUsuario").val() + "]").remove();
				$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
				$("#textobotao").text("usuario excluido")
				$("#btnEntrar").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
				
			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
				$("#textobotao").text("Erro ao excluir usuario")
				$( "#btnEntrar" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });
	
			});
		}
	}
	
	$("#float-adicionar-usuario").click(function(){		
		$("#formulario-cadastro-usuario input").each(function(){
			if ($(this).val() == '') 
				$(this).removeClass("erro");	
		});
		
		$("#link-perfil-usuario").css('display', 'none');
		$("#idUsuario").val('');
		
		$('input:checkbox').each(function(index){
			$(this).prop('checked', false);
		});
		
		$('#formulario-cadastro-usuario input').attr('disabled', false);
		$('#formulario-cadastro-usuario select').attr('disabled', false);
		
		$("#alterar").css("display", "none");
		$("#excluir").css("display", "none");
		$("#nome").val('');
		$("#email").val('');
		$("#formulario-cadastro-usuario #senha").val('');
		$('#selectTipoUsuario option[value=2]').prop('selected', true);
		$("#senha").css("display", "block");
		$("#lblSenha").css("display", "block");
		$("#btnEntrar").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"});
		$("#btnEntrar").attr('disabled', false);
		$("#textobotao").text("Confirmar")
		$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
		$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
		$("#container-formulario-cadastro-usuario").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
	});
	
	
	$("#total").click(function(){
		$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
		$("#container-formulario-cadastro-usuario").css({"animation": "none", "z-index": "-10"});
	});
	
	$("#alterar").click(function(){
		$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, 1)");
		$('#formulario-cadastro-usuario input').attr('disabled', false);
		$('#formulario-cadastro-usuario select').attr('disabled', false);
		$("#btnEntrar").attr('disabled', false);
	});
	
	$("#excluir").click(function(){
		$("#nome").css('background-color','');
		excluirUsuario();
	}); 
		 
	</script>
					