<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css" href="<c:url value="/resources/css/empresa.css" />" rel="stylesheet" />


	<section id="container-cadastro-empresa" >
		<form id="formulario" enctype="multipart/form-data">
			<div id="container-cadastro-empresa-titulo">
				<h1> Informações </h1>
			</div>
		
			<input type="hidden" id="idEmpresa">
			
			<label>Empresa</label>
			<input type="text" id="nome" name="nome" autofocus="autofocus">
			
			<label id="selecionarArquivo">Selecionar arquivo</label>
			
			<div id="descricao-holder"></div>

			<img src="" id="imagem-preview" ><br>
			
			<div id="saibaMais">
					<a onclick="carregarPaginaDaEmpresa()">Mais</a>
				</div>
			<input type="file" id="inputFile">
			
			<div  class="botoes-alterar-excluir" >
				
				<div id="alterar"  >
					
				</div>
				<div id="excluir">
					
				</div>
			</div>
			
			<!-- <div id="image-holder"></div> -->
			
			<button type="button"  onclick="verificar()" id="upload" ><span id="textobotao">Confirmar</span> </button>
			 
		</form>
	</section>	
	
	<div id="total"></div>
	<div id="float-adicionar-empresa"> 
	</div>

	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	var formData = '' ;
	var inputFile = '' ;
	
		function verificar() {			
			 if($("#idEmpresa").val() == '' ){
					 cadastrarEmpresa();
				}else{
					alterarEmpresa()
				} 
		}
		
		function cadastrarEmpresa(){
			formData = new FormData();
			inputFile = document.getElementById("inputFile");					
			
			formData.append('nome', $("#nome").val());
			formData.append('file', inputFile.files[0]);
			
			console.log(JSON.stringify(formData));
			console.log("hfaHGF")
			$.ajax({
				url : localIP + "/baseDeConhecimento/empresa",
				method : 'POST',
				data : formData,
				processData : false,
				contentType : false,
				cache : false,				
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				var tdCodigoHTML = document.createElement('td');
				$(tdCodigoHTML).text(response.id);
				
				var tdNomeHTML = document.createElement('td');
				$(tdNomeHTML ).text(response.nome);
				
				var tdDataCadastroHTML = document.createElement('td');
				$(tdDataCadastroHTML).text(response.dataCadastro);
				
				var trHTML = document.createElement('tr');
				$(trHTML).attr('data-id', response.id);
				$(trHTML).attr('onClick', 'buscarEmpresa(this)');
				
				$(trHTML).append(tdCodigoHTML);
				$(trHTML).append(tdNomeHTML);
				$(trHTML).append(tdDataCadastroHTML);
							
				$('#tabelaEmpresas tbody').append(trHTML);
				
				console.log(JSON.stringify(response))
				
				$("#textobotao").text("Empresa Cadastrada")
				$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

			}).fail(function(response, textstatus, teste) {
				verificarTipoDeErro(response);
				console.log('STATUS ' + textstatus);
				console.log(response);
				
				$("#textobotao").text("Erro ao cadastrar empresa")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });	

			});
		}
		
		function verificarTipoDeErro(erro){
			if(erro.status == 600){
				$("#nome").val("");
				$("#nome").attr({'placeholder': 'Empresa já cadastrada', 'value': ' '});		
				$("#nome").addClass("erro");
			} else {
				if(erro.status == 400){
					$("#nome").addClass("erro");
				}else{	
				}
			}
		}	
		
		function alterarEmpresa(){
			formData = new FormData();
			inputFile = document.getElementById("inputFile");
			
			formData.append('file', inputFile.files[0]);
						
			formData.append('nome', $("#nome").val());
			console.log("NOME:"+$("#nome").val())
			
			console.log("idddddddddd"+ $("#idEmpresa").val());	
			
			$.ajax({
				url : localIP + '/baseDeConhecimento/empresa/'+ $("#idEmpresa").val(),
				method: 'POST',
				data : formData,
				contentType: false,
				processData: false,
				cache : false,	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#textobotao").text("Empresa Alterada")
				$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
				
				console.log(response)

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS' + textstatus);
				console.log(response);
				
				$("#textobotao").text("Erro ao alterar empresa")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
		}
		
		function excluirEmpresa(){
			 
			var empresaObj = {
					"id": $("#idEmpresa").val(),
					"nome": $("#nome").val(),
					"imagem": $("#imagemEmpresa").val()
			}
			
			
			
		 	$.ajax({
				url : localIP + "/baseDeConhecimento/empresa/" + localStorage.getItem('busca'),
				dataType : "json",
				method: "PUT",
				contentType : "application/json",
				data: JSON.stringify( empresaObj ),
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				$("#tabelaEmpresas tbody tr[data-id=" + $("#idEmpresa").val() + "]").remove();
				
				$("#textobotao").text("Empresa desativada")
				$("#upload").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				
				$("#textobotao").text("Erro ao desativar empresa")
				$( "#upload" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			}); 
		}
		
		$('#selecionarArquivo').on('click', function() {
			  $('#inputFile').trigger('click');
			});
		
		/*Nome da imagem*/
		var nome = '';
		
		/*Caminho da imagem para exibição*/
		var caminhoImagem = '';
			
		$('#inputFile').on('change', function (event) {
			
			console.log("APAREÇA ================")
			
			/*Nome da imagem*/
			nome = $("#inputFile").val();
			
			/*Caminho da imagem para exibição*/
			caminhoImagem = URL.createObjectURL(event.target.files[0]);
	
			$('#descricao-holder').html(nome);
			$('#imagem-preview').attr('src', caminhoImagem);
			
			 if($("#idEmpresa").val() == '' ){
				 $('#descricao-holder').css('display', 'flex');
				 $('#imagem-preview').css('display', 'flex');
			}else{
				 $('#descricao-holder').css('display', 'flex');
				 $("#container-cadastro-empresa").css({"height": "370px","margin-top": "-185px"});
			} 
			
		})	;
			
		$("#float-adicionar-empresa").click(function(){
			/*Preview da imagem e descrição*/
			$('#descricao-holder').css('display', 'none');
			$('#imagem-preview').css('display', 'none');
			
			/*Tamanho container*/
			$("#container-cadastro-empresa").css({"height": "320px","margin-top": "-160px"});
			
			$("#nome").attr({'placeholder': '', 'value': ''});
			$("#container-cadastro-empresa input").removeClass('erro');
			$("#saibaMais").css("display", "none");
			$("#idEmpresa").val('');
			$("#textobotao").text("confirmar")
			$('#nome').attr('disabled', false);
			$("#alterar").css("display", "none");
			$("#excluir").css("display", "none");
			$("#nome").css('background-color','');
			$("#nome").val('');	
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
			$("#container-cadastro-empresa").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
			$("#upload").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"});

		});
		
		$("#total").click(function(){
			$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
			$("#container-cadastro-empresa").css({"animation": "none", "z-index": "-10"});
		})
				
		 $("#alterar").click(function(){
			$("#nome").css('background-color','');
			$('#nome').attr('disabled', false);
			$("#upload").attr('disabled', false);
		});
		
		$("#excluir").click(function(){
			$("#nome").css('background-color','');
			excluirEmpresa();
		}); 
		
		 $("#nome").keyup(function() {
			 if($("idEmpresa").val() != '' ){
				 $('#upload').attr('disabled', false);
			}
		});
		 
		 function carregarPaginaDaEmpresa(){
			 $(window.location.href = "../visualizar/perfilempresa.jsp").fadeIn('500');
		 }
		 	 
</script>
