<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/nivel.css" />" rel="stylesheet" />

	
	
	
	<section id="container-cadastro-nivel"  >
	
		<form action="" id="formulario-cadastro-nivel">
		
			<div id="titulo">
				<h1>Cadastro de Nivel</h1>
			</div>
			<p>
			<label class="titulos">Empresa</label>
			<input list="empresas" id="empresa">
			<datalist id="empresas">
			
			</datalist>
			</p>
			
			
			<label class="titulos">Categoria </label> 
			<input class="campos" type="text" id="categoria" list="categorias"> 
			<datalist class="campos" id="categorias"></datalist>
			
			
			<div id="container-radio-buttons" >
				<label>Nivel</label>
				
				<select id="quantidadeNiveis" >
					<option value="1" selected="selected">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
			</div>
			
			<div id="container-nivel">
			<input type="hidden" id="idNivel">
				<ul>
					<li id="li1">
						<label class="titulos" > 1º </label> 
						<input type="text" list="niveis-1" class="niveis" id="nivel1" > 
						<datalist id="niveis-1"></datalist> 
					</li>
					
					<li id="li2" style="display: none;">
						<label class="titulos" > 2º </label> 
						<input type="text" list="niveis-2" class="niveis" id="nivel2" >  
						<datalist id="niveis-2"></datalist>
					</li>	
					
					<li id="li3" style="display: none;">
						<label class="titulos" > 3º </label> 
						<input type="text" list="niveis-3" class="niveis" id="nivel3" > 
						<datalist id="niveis-3"></datalist>
					</li>
								
					<li id="liNovoNivel">	
						<label class="titulos" ><span id="numeroDoNivel">1</span>º</label>
						<input type="text" id="nomeDoNivel">
					</li>	
				</ul>
			</div>
			<button id="btnCriarNivel" type="button" onclick="verificar()"><span id="textobotao">Criar nivel</span> </button> 
		</form>
	</section>	

	<div id="total"></div>
	<div id="float-adicionar-nivel"> 
	</div>
	<script type="text/javascript">
	
	$("#quantidadeNiveis").change( function() {
		
		quantidadeNivel = $("#quantidadeNiveis option:selected").val();
				
		$("#numeroDoNivel").text($("#quantidadeNiveis option:selected").val())
		
		switch (quantidadeNivel) {
		case "1":
			$("#container-nivel #li1").css('display', 'none')
			$("#container-nivel #li2").css('display', 'none')
			$("#container-nivel #li3").css('display', 'none')
			$("#container-cadastro-nivel").css("height", "380px")
			break;
		case "2":
			$("#container-nivel #li1").css('display', 'block')
			$("#container-nivel #li2").css('display', 'none')
			$("#container-nivel #li3").css('display', 'none')
			$("#container-cadastro-nivel").css("height", "410px")
			break;
		case "3":
			$("#container-nivel #li1").css('display', 'block')
			$("#container-nivel #li2").css('display', 'block')
			$("#container-nivel #li3").css('display', 'none')
			$("#container-cadastro-nivel").css("height", "440px")
			break;
		case "4":
			$("#container-nivel #li1").css('display', 'block')
			$("#container-nivel #li2").css('display', 'block')
			$("#container-nivel #li3").css('display', 'block')
			$("#container-cadastro-nivel").css("height", "520px")
			break;		
		default:
			break;
		}
	});
	
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
		
	/* Limpar os datalist quando o usuario trocar a opção*/
	$("#empresa").on('input', function(){
		$("#categorias").empty();
		$("#categoria").val("");
		$("#nivel1").empty();
		$("#nivel2").empty();
		$("#nivel3").empty();
		console.log("limpei as categorias")
	});
	
	$("#categoria").on('input', function(){
		$("#nivel1").empty();
		$("#nivel2").empty();
		$("#nivel3").empty();
		console.log("limpei os niveis")
	});
	
	$("#nivel1").on('input', function(){
		$("#nivel2").empty();
		$("#nivel3").empty();
		console.log("limpei os niveis 3 e 2")
	});
	
	$("#nivel2").on('input', function(){
		$("#nivel3").empty();
		console.log("limpei o nivel 3")
	});
	 
	function verificar(){
		if ($("#idNivel").val() == '' ) {
			criarNivel();
		}else{
			alterarNivel();
		}
	}
	/* Criar  nivel */
	var empresa = '';
	function criarNivel(){
				
		var idNivelAnterior = '';
		var nomeDoNivel = '';
		var value = '';
		
		
		switch (quantidadeNivel) {
		
		case '1':
			value = $("#formulario-cadastro-nivel #categoria").val();
			idNivelAnterior =  $('#formulario-cadastro-nivel #categorias [value="' + value + '"]').data('id')
			break;
		case '2':
			value = $("#container-nivel #nivel1").val();
			idNivelAnterior =  $('#container-nivel #niveis-1 [value="' + value + '"]').data('id')
			break;
		case '3':
			/* idNivelAnterior = $('#nivel2').val(); */
			value = $("#container-nivel #nivel2").val();
			idNivelAnterior =  $('#container-nivel #niveis-2 [value="' + value + '"]').data('id')			
			break;
		case '4':
			value = $("#container-nivel #nivel3").val();
			idNivelAnterior =  $('#container-nivel #niveis-3 [value="' + value + '"]').data('id')
			break;
		}
		
		var usuarioObj = {
				"id" :  localStorage.getItem("usuario_id")
			}; 
			
		value = $("#formulario-cadastro-nivel #empresa").val();
		empresa =  $('#formulario-cadastro-nivel #empresas [value="' + value + '"]').data('id')
		var empresaObj = {
				"id" : empresa
			};
		
		var categoriaObj = {
				"id" : idNivelAnterior
			};
		
		var nivelObj = {
				"nome": $("#nomeDoNivel").val(),
				"empresa": empresaObj,
				"categoria": categoriaObj,
				"usuario": usuarioObj,
		}
		 
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria",
			dataType: "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify ( nivelObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			
			console.log(JSON.stringify(nivelObj))
			$("#textobotao").text("sucessso")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
		}).fail(function(response, textstatus, teste){
			console.log(JSON.stringify(nivelObj))
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			console.log("CODIGO = " + textstatus);
			
			$("#textobotao").text("Erro")
			$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });
		});
		
	}
	
	
	//Popular o datalist com as empresas do banco
	var options = '';
	var quantidadeNivel = '';
	$(document).ready(function(){
		
		quantidadeNivel = $("#quantidadeNiveis option:selected").val();
		/* quantidadeNivel = $('input[name=quantidadeNivel]:checked', 'form').val(); */

		$("#radioExterno1").css("border", "5px solid #73BFCB");
		$("#radioInterno1").css({"background-color": "#73BFCB", "border": "5px solid #73BFCB"});
		
		console.log(localStorage.getItem("token"))
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){
				
				options =  '<option>' + response[index].nome + '</option>';
				
				$("#formulario-cadastro-nivel #empresas").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
	});
	
	
	/*Popular o datalist com as categorias da empresa escolhida*/
	$("#formulario-cadastro-nivel #empresa").on('input', function(){
		console.log("haha")
		/* var empresaID =  $('#empresa').val() */
			var value = $("#formulario-cadastro-nivel #empresa").val();
		  var empresaID  = $('#formulario-cadastro-nivel #empresas [value="' + value + '"]').data('id')
		console.log("Entrei na categoria ");
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/empresa/" + empresaID ,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$("#formulario-cadastro-nivel #categorias").empty(); 	
			$.each(response, function(index){
				console.log("Listei as categorias")
				
				$("#formulario-cadastro-nivel #categorias").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro na categoria");
		});
	});
	
	/* Popular o datalist de nivel 1 com os niveis da categoria escolhida */
	$("#formulario-cadastro-nivel #categoria").on('input', function(){
		
		var value = $("#formulario-cadastro-nivel #categoria").val();
		var categoriaID = $('#formulario-cadastro-nivel #categorias [value="' + value + '"]').data('id')
		console.log("Entrei no nivel 1");
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + categoriaID,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$("#niveis-1").empty(); 	
			$.each(response, function(index){
				console.log("Listei o nivel 1")
				
				$("#niveis-1").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 1");
		});
	});
	
	/* Popular o  datalist de nivel 2  */
	$("#container-nivel #nivel1").on('input', function(){
		console.log("Entrei no nivel 2")
		
		var value = $("#container-nivel  #nivel1").val();
		var nivel = $('#container-nivel  #niveis-1 [value="' + value + '"]').data('id')
		
		console.log("Nivel ===== "  + nivel)
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$("#container-nivel #niveis-2").empty(); 
			$.each(response, function(index){
				console.log("Listei o nivel 2")
				/* $("#niveis-2").append("<option value = '" + response[index].id + "'>" + response[index].nome + "</option>" ); */
				
				$("#container-nivel #niveis-2").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 2")
		});
	});
	
	/* Popular o datalist de  nivel 3 */
	$("#container-nivel #nivel2").on('input', function(){
		
		var value = $("#container-nivel #nivel2").val();
		var nivel = $('#container-nivel #niveis-2 [value="' + value + '"]').data('id')
		console.log("nivel = " + nivel)
		/* console.log('#niveis2 [value="' + value + '"]').data('value')); */
		console.log("entrei no nivel 3 ");
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
			dataType: "Json",
			method: "GET",
			contentType : "application/json",
			headers :{
				"Authorization": localStorage.getItem("token")
			}
		}).done(function(response){
			$("#niveis-3").empty();
			$.each(response, function(index){
				console.log("Listei o nivel 3")
				
				$("#container-nivel #niveis-3").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
			});
				
		}).fail(function(response, textstatus, teste){
			console.log("erro no nivel 3")
		});
	});
	
	$("#float-adicionar-nivel").click(function(){
		$("#container-cadastro-nivel").css("height", '380px')
		$("#formulario-cadastro-nivel label").css("display", '');
		$("#formulario-cadastro-nivel input").css("display", '');
		$("#formulario-cadastro-nivel select").css("display", '');
		$("#formulario-cadastro-nivel li").css("display", '');
		$("#container-radio-buttons").css("display", '');
		
		$("#liNovoNivel").css("display", '');
		$("#nomeDoNivel").css({"display": "block", "border": '', "margin-top": '-15px', "margin-left": '15px'});
		$("#idNivel").val('');
		$("#nomeDoNivel").val('');
						
		$("#formulario-cadastro-nivel #btnCriarNivel #textobotao").text("confirmar")
		
		$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
		$("#container-cadastro-nivel").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
		
		$( "#formulario-cadastro-nivel #btnCriarNivel" ).css({"box-shadow": "", "border-top": "" });
		
	});
	
	$("#total").click(function(){
		$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
		$("#container-cadastro-nivel").css({"animation": "none", "z-index": "-10"});
	});
		
	
	function alterarNivel(){
		value = $("#container-categoria-empresa #datalistEmpresas").val();
		empresa =  $('#container-categoria-empresa #listaEmpresas [value="' + value + '"]').data('id')
		var empresaObj = {
				"id" : empresa
			};
		
		var nomeDoNivel = '';

		var usuarioObj = {
				"id" :  localStorage.getItem("usuario_id")
			}; 
		
		var nivelObj = {	
				"id": $("#idNivel").val(),
				"nome": $("#nomeDoNivel").val(),
				"usuario": usuarioObj,
				"empresa": empresaObj,
		}
		 console.log(JSON.stringify(nivelObj))
		 
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria",
			dataType: "json",
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify ( nivelObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			
			console.log(JSON.stringify(nivelObj))
			$("#formulario-cadastro-nivel #textobotao").text("Nivel Alterado")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
		}).fail(function(response, textstatus, teste){
			console.log(JSON.stringify(nivelObj))
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			console.log("CODIGO = " + textstatus);
			
			$("#textobotao").text("Erro")
			$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });
		});
	}
	</script>

</html>