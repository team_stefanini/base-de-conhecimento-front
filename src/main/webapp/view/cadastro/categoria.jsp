<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/categoria.css" />" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">


	<section id="container-cadastro-categoria" >
		<form id="formulario-cadastro-categoria">
			<div id="titulo">
				<h1>Informações</h1>
			</div>
			<label>Empresa</label>
			<input list="empresas" id="empresa">
			<datalist id="empresas"></datalist>
			
			<input type="hidden" id="idEmpresa">
			<input type="hidden" id="idCategoria">
			<input type="hidden" id="tituloCategoria">
			
			<label>Nome categoria</label>
			<input type="text" id="categoria">
			
			<div  class="botoes-alterar-excluir" >
				<div id="alterar"  >
					
				</div>
				<div id="excluir">
					
				</div>
			</div>
			
			<button type="button" onclick="criarCategoria()" id="btnCriarCategoria"><span id="textobotao">Criar categoria</span> </button> 
			
		</form>
	</section>	
	
	<div id="total"></div>
	<div id="float-adicionar-categoria"> 
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	//Popular o datalist com as empresas do banco
	var options = '';
	
	$(document).ready(function(){
		
		console.log(localStorage.getItem("token"))
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){
				
				options =  '<option>' + response[index].nome + '</option>';
				
				$("#empresas").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
				
			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
	});
	
	var usuarioObj = "";
	var empresaObj = "";
	var categoriaObj = "";
	
	
	function verificarCampos(){
		
		$("#formulario-cadastro-categoria input").each(function(){
			if ($(this).val() == ''){ 
				$(this).addClass("erro");
			}else{
				$(this).removeClass("erro");
			}	
		});
	}
	
	/* Criar Categoria */
	function criarCategoria(){
		
		verificarCampos();
		 
		if($("#idCategoria").val() == ''){
			console.log("CADASTRO");
			cadastrarCategoria();
		}else{
			console.log("ALTERAR");
			alterarCategoria();
		}
	}
		 console.log(categoriaObj)
	
	function cadastrarCategoria(){
			 var value = $("#empresa").val();
			 var empresaID = $('#empresas [value="' + value + '"]').data('id'); 
		usuarioObj = {
				"id" :  localStorage.getItem("usuario_id")
			}; 
			
			empresaObj = {
				"id" :  empresaID
			};
			
			console.log("Nome = " + $("#categoria").val())
			
			categoriaObj = {
					"nome": $("#categoria").val(),
					"empresa": empresaObj,
					"usuario": usuarioObj
			}	
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria",
			dataType: "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify (categoriaObj),
			headers : {
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste){
			$("#textobotao").text("sucessso")
			$("#btnCriarCategoria").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"});
			console.log(JSON.stringify(categoriaObj))
		}).fail(function(response, textstatus, teste){
			verificarTipoDeErro(response);
		});	
	}
	
	function verificarTipoDeErro(erro){
		if(erro.status == 600){
			$("#categoria").val("");
			$("#categoria").attr({'placeholder': 'Categoria já cadastrado', 'value': ' '});		
			$("#categoria").addClass("erro");
			console.log("_____________________________________________ Categoria igual")
		} else {
			if(erro.status == 500){
				$("#categoria").addClass("erro");
				$("#empresa").addClass("erro");
			}else{
				console.log("_________________________ Segue o jogo no beira rio!")	
			}
		}
	}
		 
		 
	function alterarCategoria() {	
		
		verificarCampos();
		
		usuarioObj = {
				"id" :  localStorage.getItem("usuario_id")
			}; 
			
		empresaObj = {
				"id" : $('#idEmpresa').val()
		};
			
			
		categoriaObj = {
				"id": $("#idCategoria").val(),
				"nome": $("#categoria").val(),
				"empresa": empresaObj,
				"usuario": usuarioObj
		}	
		
		
		console.log("empresa = " + $('#idEmpresa').val())
		console.log("id da categoria = " + $("#idCategoria").val())
		console.log("Nome = " + $("#categoria").val())
		
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria",
			method: "PUT",
			dataType: "json",
			contentType: "application/json",
			data: JSON.stringify( categoriaObj ),
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textestatus, teste) {
			console.log("ALTERADAAAAAAAAAAAAAAAAAAAAAAAAAAA")
			console.log("CATEGOGIAAAAAAAAAAAAAAAAAAAA:"+ response.id)
			console.log(teste.status)
			
			$("#textobotao").text("sucessso")
			$("#btnCriarCategoria").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
			
		}).fail(function(response, textestatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
		})
	}	 
	
	function excluirCategoria(){
		usuarioObj = {
				"id" :  localStorage.getItem("usuario_id")
			}; 
			
		empresaObj = {
				"id" : $('#idEmpresa').val()
		};
			
		 
		var categoriaObj = {
				"id": $("#idCategoria").val(),
				"nome": $("#tituloCategoria").val(),
				"empresa": empresaObj,
				"usuario": usuarioObj
		}
		
		console.log($("#tituloCategoria").val())
		
		
		$.ajax({
			url : localIP + "/baseDeConhecimento/categoria/" + localStorage.getItem('busca'),
			dataType : "json",
			method: "PUT",
			data: JSON.stringify(categoriaObj),
			contentType : "application/json",
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			$("#textobotao").text("Categoria desativada");
			$("#btnCriarCategoria").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"});

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
			
			$("#textobotao").text("Erro ao desativar categoria");
			$( "#btnCriarCategoria" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

		});
	}
	
	$("input[type='radio'][name='quantidadeEmpresa']").click(
            function(){
            	quantidadeEmpresa = $('input[name=quantidadeEmpresa]:checked', '#formulario-cadastro-usuario').val();
            	
            	console.log(quantidadeEmpresa);
            	
            	switch (quantidadeEmpresa) {
            	
            	
					case "1":
						console.log("BBB");
							$("#empresa1").css("display", "block");
						break;

					case "2":
							$("#empresa1").css("display", "block");
							$("#empresa2").css("display", "block");
						break;
						
					case "3":
						$("#empresa1").css("display", "block");
						$("#empresa2").css("display", "block");
						$("#empresa3").css("display", "block");
						break;
					}
            }
        );
	
	$("#float-adicionar-categoria").click(function(){
		
		$("#container-cadastro-categoria input").removeClass('erro');
		$("#idEmpresa").val("");
		$("#empresa").val("");
		$('#categoria').attr('disabled', false);
		$('#empresa').attr('disabled', false);
		$("#categoria").val("");
		$("#categoria").css('background-color','');
		$("#alterar").css("display", "none");
		$("#excluir").css("display", "none");
		$("#textobotao").text("Confirmar")
		$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
		$("#container-cadastro-categoria").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
		$("#btnCriarCategoria").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"})
		
	});
	
	$("#total").click(function(){
		$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
		$("#container-cadastro-categoria").css({"animation": "none", "z-index": "-10"});
	});
	
	 $("#alterar").click(function(){
			$("#categoria").css('background-color','');
			$('#categoria').attr('disabled', false);
			$("#empresa").css('background-color','');
			$('#empresa').attr('disabled', false);
	});
	 
	$("#excluir").click(function(){
		$("#nome").css('background-color','');
		$("#empresa").css('background-color','');
		excluirCategoria();
	}); 
	
</script>
