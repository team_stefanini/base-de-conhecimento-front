<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css" href="<c:url value="/resources/css/gruposolucao.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<section id="container-cadastro-gruposolucao" >
		<form id="formulario">
			<div id="titulo">
				<h1>Informações</h1>
			</div>
			
			<input type="hidden" id="idGrupo">
			
			<label>Nome</label>
			<input type="text" id="nome" name="nome" autofocus="autofocus">
			
			<button id='btnCriarGrupo' type="button"  onclick="verificar()" ><span id="textobotao">Criar Grupo</span> </button> 
		</form>
		
		<div  class="botoes-alterar-excluir" >
			<div id="alterar"  >
				
			</div>
			<div id="excluir">
				
			</div>
		</div>
	</section>	
	
	<div id="total">
	</div>
	<div id="float-adicionar-grupo"> 
	</div>
	
	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	function verificar() {			
		 if($("#idGrupo").val() == '' ){
			 	criarGrupoSolucao();
			}else{
				alterarGrupoSolucao();
			} 
	}
	
	function verificarCampos(){
		$("#formulario input").each(function(){
			if ($(this).val() == ''){ 
				$(this).addClass("erro");
			}else{
				$(this).removeClass("erro");
			}	
		});
	}

	function criarGrupoSolucao() {
		verificarCampos();
		
		var grupoSolucaoObj = { 
				"nome" : $('#nome').val()	
			}				

		$.ajax({
			url : localIP + "/baseDeConhecimento/grupo",
			dataType : "json",
			method : 'POST',
			contentType : "application/json",
			data: JSON.stringify ( grupoSolucaoObj ),
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			
			
			var tdCodigoHTML = document.createElement('td');
			$(tdCodigoHTML).text(response.id);
			
			var tdNomeHTML = document.createElement('td');
			$(tdNomeHTML ).text(response.nome);
			
			var tdDataCadastroHTML = document.createElement('td');
			$(tdDataCadastroHTML).text(response.dataCadastro);
			
			var trHTML = document.createElement('tr');
			$(trHTML).attr('data-id', response.id);
			$(trHTML).attr('onClick', 'buscarGrupo(this)');
			
			$(trHTML).append(tdCodigoHTML);
			$(trHTML).append(tdNomeHTML);
			$(trHTML).append(tdDataCadastroHTML);
						
			$('#tabelaGrupos tbody').append(trHTML);
			
			$("#textobotao").text("grupo cadastrado")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
			
			verificarTipoDeErro(response);
			
		});
	}
	
	
	function alterarGrupoSolucao(){
		verificarCampos();
		
		var grupoSolucaoObj = { 
				"id": $('#idGrupo').val(),
				"nome" : $('#nome').val()	
			}				

		console.log("OBJETO GRUPO ==============")
		console.log(grupoSolucaoObj)
		
		$.ajax({
			url : localIP + "/baseDeConhecimento/grupo",
			dataType : "json",
			method : "PUT",
			contentType : "application/json",
			data: JSON.stringify ( grupoSolucaoObj ),
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			$("#textobotao").text("grupo alterado")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
			
			verificarTipoDeErro(response);
			
		});
	}
	
	function verificarTipoDeErro(erro){
		if(erro.status == 600){
			$("#nome").val("");
			$("#nome").attr({'placeholder': 'Grupo de Solução já cadastrado', 'value': ' '});		
			$("#nome").addClass("erro");
			console.log("_____________________________________________ Grupo igual")
		} else {
			console.log("_________________________ segue o jogo no beira rio!")
		}
	}
	
	function excluirGrupo(){		
		$.ajax({
			url : localIP + "/baseDeConhecimento/grupo/" + localStorage.getItem("busca"),
			dataType : "json",
			method : "DELETE",
			contentType : "application/json",
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {
			$("#textobotao").text("grupo excluido")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
			
			$("#tabelaGrupos tbody tr[data-id=" + $("#idGrupo").val() + "]").remove();

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);
		
		});
	}
	
	
	$("#float-adicionar-grupo").click(function(){
		$("button").css({"box-shadow": "", "border-top": ""})
		$("#nome").val('');
		$("#textobotao").text("confirmar")
		$('#nome').attr('disabled', false);
		$("#alterar").css("display", "none");
		$("#excluir").css("display", "none");
		$("#nome").css('background-color','');
		$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
		$("#container-cadastro-gruposolucao").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});
	});
	
	$("#total").click(function(){
		$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
		$("#container-cadastro-gruposolucao").css({"animation": "none", "z-index": "-10"});
	})
	
	$("#alterar").click(function(){
		$("#nome").css('background-color','');
		$('#nome').attr('disabled', false);
	});
	
	$("#excluir").click(function(){
		$("#nome").css('background-color','');
		excluirGrupo();
	}); 	
	
	</script>
</body>
</html>