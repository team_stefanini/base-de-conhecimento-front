<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html >
<html >
<meta charset="utf-8" lang="pt-br">

<head>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/analista.css" />">
	<title> Analista </title>
</head>
<body class="fadeIn">
<c:import url="../templates/header.jsp"></c:import>
<c:import url="../templates/aviso.jsp" />
<c:import url="../visualizar/perfil.jsp" />

<section id="container-procedimentos">
		
	<div id="container-empresas">
			<ul id="empresasDoUsuario">
					
			</ul>
			
		</div>

		<div id="container-pesquisa">
			<div id="container-niveis">
				<input class="niveis-input" placeholder="Categoria " list="categorias" id="categoria">
				<datalist id="categorias">
				</datalist>
			
				<input class="niveis-input" placeholder="nivel 1 " list="niveis1" id="nivel1">
				<datalist id="niveis1">

				</datalist>
				<input class="niveis-input" placeholder="nivel 2 " list="niveis2" id="nivel2">
				<datalist id="niveis2">

				</datalist>
				<input class="niveis-input" placeholder="nivel 3 " list="niveis3" id="nivel3">
				<datalist id="niveis3">

				</datalist>
				<input class="niveis-input" placeholder="nivel 4 " list="niveis4" id="nivel4">
				<datalist id="niveis4">

				</datalist>
				
			</div>	

			 <input	class="pesquisa" id="input-pesquisa" placeholder="Pesquisar ...">
			 
			 <label id="container-btnSwitch-procedimentos-ativos-desativos" for="btnSwitch-procedimentos-ativos-desativos" >
			 	<input type="checkbox" id='btnSwitch-procedimentos-ativos-desativos' checked="checked" >
			 	<div id="divSwitch-procedimentos-ativos-desativos" class="btnAtivados">
			 	</div>
			 </label>
			 
			 <span id="msgm-procedimentos-ativados-desativados">Ativos</span>
			 
			 <div id="container-tabela-pesquisa">
			 
				<table id="tabelaProcedimentos" class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Versão</th>
							<th>Data</th>
							<th>Titulo</th>
						</tr>
					</thead>
					<tbody id="corpoTabela"> 
					</tbody>
				</table>
		</div>
	</div>
	

</section>

<script>
	//SCRIPT PRA SABER QUANDO O USUARIO CHEGOU AO FINAL DA PÁGINA
	//window.onscroll = function(ev) {
	//    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
	//      // you're at the bottom of the page
	//      console.log("Bottom of page");
	//    }
	//};

	
	var idCategoriaProcedimento = '';

	
		var tdHTML = '';
		var trHTML = '';
		
		/*Traz todos os procedimentos das empresas atrelada ao usuario*/
	$(document).ready(function (){
		
		if (localStorage.getItem("usuario_tipo") == "ANALISTA") {
			$("#container-btnSwitch-procedimentos-ativos-desativos").css("display", "none");
			$("#msgm-procedimentos-ativados-desativados").css("display", "none");
		}
		
		
		carregarProcedimentosAtivos();
				
				/*Para o perfil de usuarios*/
				if(localStorage.getItem("usuario_tipo") !== "ADMINISTRADOR"){
					$.ajax({
						url: localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("usuario_id"),
						dataType: "json",
						method: "GET",
						contentType: "application/json",
						headers : {
							"Authorization" : localStorage.getItem("token")
							},
					}).done(function(response, textstatus, teste){
						
						$.each(response.empresa, function(index){
							$("#informacoes ul").append("<li>" + response.empresa[index].nome + ". </li>" ); 
						
							
							$("#empresasDoUsuario").append("<li>" + "<img title=" + response.empresa[index].nome + " class='empresalogo' src="+ localIP +"/baseDeConhecimento" + response.empresa[index].imagemLogo.caminho + 
									" data-empresaID=" + response.empresa[index].id + ">" + "</li>" );
							
						});
						
						onClickLI();
						
					}).fail(function(response, textstatus, teste){
						console.log("teste = " + teste.status);
						console.log("response = " + response);
						console.log("CODIGO = " + textstatus);
				});
					
			}else{
					/*Lista Imagens para o perfil de ADM*/
					$.ajax({
						url: localIP + "/baseDeConhecimento/empresa",
						dataType: "json",
						method: "GET",
						contentType: "application/json",
						headers : {
							"Authorization" : localStorage.getItem("token")
							},
					}).done(function(response, textstatus, teste){
						
						$.each(response, function(index){
							$("#informacoes ul").append("<li>" + response[index].nome + ". </li>" );
							var caminho = "";
							
							if(response[index].imagemLogo == null){
								caminho= "/logoEmpresa/noimage.svg";
							}else{
								caminho= response[index].imagemLogo.caminho;
							}
							
							$("#empresasDoUsuario").append("<li>" + "<img  title=" + response[index].nome + " class='empresalogo' src="+ localIP +"/baseDeConhecimento" + caminho + 
									" data-empresaID=" + response[index].id + ">" + "</li>" );
							
						});
						
						onClickLI();
						
					}).fail(function(response, textstatus, teste){
						console.log("teste = " + teste.status);
						console.log("response = " + response);
						console.log("CODIGO = " + textstatus);
				});
			}		
	});
				
		function buscarProcedimento(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			setTimeout(buscar(), 500);
		}
		
		function buscar(){
			$(window.location.href = "../visualizar/procedimento.jsp").fadeIn('500');
		}
		
		function pesquisar(){
			var $rows = $('#corpoTabela tr');
			$('#input-pesquisa').keyup(function() {
				
			  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');
	
			  $rows.hide().filter(function() {
			    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			    var matchesSearch = true;
			    $(val).each(function(index, value) {
			      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
			    });
			    return matchesSearch;
			  }).show();
			});
		}
		
		/* Quando o usuario clicar em uma empresa vai preencher o datalist com as categorias dela */
		var empresaID = '';
		function onClickLI(){
			$('#container-empresas .empresalogo').click(function(){
				
				$("#categoria").val('');
				$("#nivel1").val('');
				$("#nivel2").val('');
				$("#nivel3").val('');
				$("#nivel4").val('');
				
			     $('.empresalogo').css('height', '50px');
			    $(this).css('height', '70px');    
			    
			    empresaID =  $(this).attr("data-empresaID");
			    
			    $.ajax({
					url: localIP + "/baseDeConhecimento/categoria/empresa/" + empresaID,
					dataType: "json",
					method: "GET",
					contentType: "application/json",
					headers : {
						"Authorization" : localStorage.getItem("token")
						}
					}).done(function( response){
						$("#categorias").empty();
						$("#categoria").focus();
						$("#niveis1").empty();
						$("#niveis2").empty();
						$("#niveis3").empty();
						$("#niveis4").empty();
						$.each(response, function(index){
							options =  '<option>' + response[index].nome + '</option>';
							
							$("#categorias").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
						});
						
					}).fail(function(response, textstatus, teste){
						console.log(teste.statu);
						console.log(response.status);
						console.log(textstatus);
					});
			    

			    $('#corpoTabela').slideUp('fast');
				$("#corpoTabela").empty() 
			    $.ajax({
			    	url : localIP + "/baseDeConhecimento/procedimentoPorEmpresa/" + empresaID,
					dataType : "json",
					method : "GET",
					contentType : "application/json",
					headers: {
						"Authorization" : localStorage.getItem("token"),}
					}).done(function(response) {
					//esvazia o corpo da tabela
					 //pra cada objeto no array faz a funçao
						$.each(	response, function(index) {
						//devolve as informações de cada um dos objetos encontrados
						//index representa a posicao do objeto... ex: Objeto na psição 2
						// e o atributo desejada
						tdHTML = '<td>' + response[index].id + '</td>' + 
						'<td>' + response[index].versao + '</td>' +
						'<td>' + response[index].vencimento + '</td>' +
						'<td>' + response[index].titulo + '</td>';
						trHTML = '<tr data-id=' + response[index].id + " onclick='buscarProcedimento(this)'>" + tdHTML + '</tr>';

						$('#tabelaProcedimentos tbody').append(trHTML);
						$('#corpoTabela').slideDown('fast');
						$(trHTML).show('slow');
						
						});		
						pesquisar();
					});
			});
		}
		
		
		$("#categoria").on('input', function(){
			
			var value = $("#categoria").val();
			var categoriaID = $('#categorias [value="' + value + '"]').data('id')
			
			idCategoriaProcedimento = categoriaID;
			carregarProcedimentoPorCategoria();
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + categoriaID,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				$("#niveis1").empty();
				$("#nivel1").focus();
				$("#niveis2").empty();
				$("#niveis3").empty();
				$("#niveis4").empty();
				$.each(response, function(index){
					
					$("#niveis1").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
					
			}).fail(function(response, textstatus, teste){
				console.log("erro no nivel 1");
			});
			
			
		});
		
		/* Popular o  datalist de nivel 2  */
		$("#nivel1").on('input', function(){
			console.log("Entrei no nivel 2")
			
			var value = $("#nivel1").val();
			var nivel = $('#niveis1 [value="' + value + '"]').data('id')
			idCategoriaProcedimento = nivel;
			carregarProcedimentoPorCategoria();
			
			console.log("Nivel ===== "  + nivel)
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				$("#niveis2").empty();
				$("#nivel2").focus();
				$("#niveis3").empty();
				$("#niveis4").empty();
				$.each(response, function(index){
					console.log("Listei o nivel 2")
					/* $("#niveis-2").append("<option value = '" + response[index].id + "'>" + response[index].nome + "</option>" ); */
					
					$("#niveis2").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
					
			}).fail(function(response, textstatus, teste){
				console.log("erro no nivel 2")
			});
		});
		
		/* Popular o datalist de  nivel 3 */
		$("#nivel2").on('input', function(){
			
			var value = $("#nivel2").val();
			var nivel = $('#niveis2 [value="' + value + '"]').data('id')
			idCategoriaProcedimento = nivel;
			carregarProcedimentoPorCategoria();
			
			/* console.log('#niveis2 [value="' + value + '"]').data('value')); */
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				$("#niveis3").empty();
				$("#nivel3").focus();
				$("#niveis4").empty();
				$.each(response, function(index){
					
					$("#niveis3").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
					
			}).fail(function(response, textstatus, teste){
				console.log("erro no nivel 3")
			});
		});
		
		$("#nivel3").on('input', function(){
			
			var value = $("#nivel3").val();
			var nivel = $('#niveis3 [value="' + value + '"]').data('id')
			idCategoriaProcedimento = nivel;
			carregarProcedimentoPorCategoria();
			
			console.log("nivel = " + nivel)
			/* console.log('#niveis2 [value="' + value + '"]').data('value')); */
			console.log("entrei no nivel 4 ");
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				$("#niveis4").empty();
				$("#nivel4").focus();
				$.each(response, function(index){
					console.log("Listei o nivel 3")
					
					$("#niveis4").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
					
			}).fail(function(response, textstatus, teste){
				console.log("erro no nivel 3")
			});
		});
		
		$("#nivel4").on('input', function(){
			var value = $("#nivel4").val();
			var nivel = $('#niveis4 [value="' + value + '"]').data('id')
			
			idCategoriaProcedimento = nivel;
			carregarProcedimentoPorCategoria();
			
			$("#input-pesquisa").focus();
		});
		
		function carregarProcedimentoPorCategoria(){
			/*Carregar os procedimentos da categoria escolhida*/
			$.ajax({
				url: localIP + "/baseDeConhecimento/procedimentoPorCategoria/" + idCategoriaProcedimento,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				console.log("procedimento")
				//esvazia o corpo da tabela
				 $("#corpoTabela").empty() 
				 //pra cada objeto no array faz a funçao
					$.each(	response, function(index) {
						console.log("procedimento")
					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					tdHTML = '<td>' + response[index].id + '</td>' +  '<td>' + response[index].versao + '</td>' + '<td>' + response[index].vencimento + '</td>' +

					'<td>' + response[index].titulo + '</td>';
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarProcedimento(this)'>" + tdHTML + '</tr>';

					$('#tabelaProcedimentos tbody').append(trHTML);
					$(trHTML).show('slow');
					
					});
				
				pesquisar();
			}).fail(function(response, textstatus, teste){
				console.log(response.status)
			});
		}

	 	$("#btnSwitch-procedimentos-ativos-desativos").change(function(event){
		    if (this.checked){
		    	$("#container-btnSwitch-procedimentos-ativos-desativos").css('background-color', '#73bfcb');
		    	 $("#divSwitch-procedimentos-ativos-desativos").css('margin-left', '0px');
		    	 carregarProcedimentosAtivos();
		    	 $("#msgm-procedimentos-ativados-desativados").text('Ativos');
		    	$(".niveis-input").attr('disabled', false);
		    	$(".niveis-input").css('background-color', '#fff');
		    } else {
		    	$("#divSwitch-procedimentos-ativos-desativos").css('margin-left', '30px');
		    	$("#container-btnSwitch-procedimentos-ativos-desativos").css('background-color', 'rgba(220, 220, 220, 1)');
		        carregarProcedimentosInativos();
		    	$("#msgm-procedimentos-ativados-desativados").text('Inativos');
		    	$(".niveis-input").attr('disabled', true);
		    	$(".niveis-input").css('background-color', 'rgba(220, 220, 220, .4)');
		    }
	}); 	
	 	
	 function carregarProcedimentosAtivos(){
		 $('#tabelaProcedimentos tbody').slideUp('fast');
			var trHTML = '';
			
				$.ajax({url : localIP + "/baseDeConhecimento/procedimento/",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers: {"Authorization" : localStorage.getItem("token")},
				}).done(function(response) {
				//conta quantos objetos tem no array
					$.each(	response, function(index) {
					tdHTML = '<td>' + response[index].id + '</td>' + 
					'<td>' + response[index].versao + '</td>' +
					'<td>' + response[index].vencimento + '</td>' +
					'<td>' + response[index].titulo + '</td>';
					trHTML += '<tr data-id=' + response[index].id + " onclick='buscarProcedimento(this)'>" + tdHTML + '</tr>';
					});	
					$('#tabelaProcedimentos tbody').empty();
					$('#tabelaProcedimentos tbody').append(trHTML).slideDown();
					
					pesquisar()
				});
				
	 }
	
	 
	function carregarProcedimentosInativos(){
		$('#tabelaProcedimentos tbody').slideUp('fast');
			var trHTML = '';
			$.ajax({url : localIP + "/baseDeConhecimento/procedimento/inativos",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers: {"Authorization" : localStorage.getItem("token")},
				}).done(function(response) {
					$.each(	response, function(index) {
					
					tdHTML = '<td>' + response[index].id + '</td>' + 
					'<td>' + response[index].versao + '</td>' +
					'<td>' + response[index].vencimento + '</td>' +
					'<td>' + response[index].titulo + '</td>';
					trHTML += '<tr data-id=' + response[index].id + " onclick='buscarProcedimento(this)'>" + tdHTML + '</tr>';
					});
					$('#tabelaProcedimentos tbody').empty();
					$('#tabelaProcedimentos tbody').append(trHTML).slideDown();				
					
					pesquisar()
				});
			
		}
	
		
</script>
</body>
</html>