<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css"  href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css"  href="<c:url value="/resources/css/visualizarprocedimento.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<title>Stefanini</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<div id="container-visualizar-procedimento">
				<input type="hidden" id="idProcedimento">
				<h1 id="tituloProcedimento"></h1>
				<input type="file" id="fileUpload" name="fileUpload" multiple="multiple">
				<input type="button" id="btnEnviar" value="Enviar" />
	</div>

	<script>
	
	$(function () {
	    var form;
	    $('#fileUpload').change(function (event) {
	        form = new FormData();
	        form.append('id:', 1);
	        console.log(event.target.files)
	        $(event.target.files).each(function() {
				form.append('file', $(this)[0]);
			});
	    });

	    $('#btnEnviar').click(function () {
	        $.ajax({
	            url: "http://192.168.2.245/baseDeConhecimento/empresa/" + 1 + "/imagem", 
	            data: form,
	            processData: false,
	            contentType: false,
	            type: 'POST',
	            headers : {					
					"Authorization" : localStorage.getItem("token")
				},
	            success: function (data) {
	                console.log("deu certo")
	            }
	        });
	    });
	});
	
	
	</script>
</body>
</html>