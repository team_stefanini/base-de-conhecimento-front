<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css" href="<c:url value="/resources/css/empresa.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<section id="container-cadastro-empresa" >
		<form id="formulario" enctype="multipart/form-data">
			<div id="container-cadastro-empresa-titulo">
				<h1> Empresa </h1>
			</div>
			<label>Empresa</label>
			<input type="text" id="nome" name="nome" autofocus="autofocus">
			
			<label id="selecionarArquivo">Selecionar arquivo</label>
			<input type="file" id="inputFile" name="file" >
			
			<!-- <div id="image-holder"></div>  -->
			
			<button type="button"  onclick="criarEmpresa()" id="upload" ><span id="textobotao">Alterar Empresa</span> </button> 
		</form>
	</section>	
	
	<div id="total"></div>
	<div id="float-adicionar-empresa"> 
	</div>

	<script>

		function criarEmpresa() {
			var formData = new FormData();
			var inputFile = document.getElementById("inputFile");					
			
			formData.append('nome', $("#nome").val());
			formData.append('file', inputFile.files[0]);

			console.log("ENTREI");

			$.ajax({
				url : "http://192.168.2.245/baseDeConhecimento/empresa",
				type : 'POST',
				data : formData,
				processData : false,
				contentType : false,
				cache : false,				
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#textobotao").text("sucessso")
				$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
		}
		
		$('#selecionarArquivo').on('click', function() {
			  $('#inputFile').trigger('click');
			})
			
		$("#float-adicionar-empresa").click(function(){
			$("#nome").val('');	
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"});
			$("#container-cadastro-empresa").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"});

		});
		
		$("#total").click(function(){
			$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
			$("#container-cadastro-empresa").css({"animation": "none", "z-index": "-10"});
		})
	</script>
