<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />"	rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/usuarios.css" />" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../cadastro/usuario.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	<c:import url="../templates/aviso.jsp" />
	
	<section id="container-usuarios">
		<div id="titulo">
			<h1>Usuarios</h1>
		</div>
		
		<div id="container-pesquisa">
			<label> <input class="pesquisa" id="input-pesquisa" placeholder="Digite a sua pesquisa ..."></label>
		</div>

		<div id="container-tabela-pesquisa">
			<table id="tabelaUsuarios" class="table">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Tipo</th>
						<th>Empresas</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody id="corpoTabela">
				
				</tbody>
			</table>
		</div>
	</section>

	<script type="text/javascript">
	
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
		var tdHTML = '';
		var trHTML = ''; 
		var empresas = '';
		
		
		$(document).ready(function() {
			carregarTabelaDeUsuarios();
		});
		

		
		function escapeHtml(unsafe) {
		    return unsafe
		         .replace(/&/g, "&amp;")
		         .replace(/</g, "&lt;")
		         .replace(/>/g, "&gt;")
		         .replace(/"/g, "&quot;")
		         .replace(/'/g, "&#039;");
		 }
		
		function carregarTabelaDeUsuarios(){
			
			$('#tabelaUsuarios tbody').empty();
				
			$.ajax({
				url : localIP + "/baseDeConhecimento/usuario",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers : {
				"Authorization" : localStorage.getItem("token")
					},
				}).done(function(response) {
					console.log(response);
					//conta quantos objetos tem no array
					var tdHTML = '';
					var trHTML = ''; 
					$.each(response,function(index) {
						//devolve as informações de cada um dos objetos encontrados
						//index representa a posicao do objeto... ex: Objeto na psição 2
						// e o atributo desejada
						
						var empresa = response[index].empresa;
						console.log(response[index].nome);
						//tdHTML = "<a href=" + response[index].id  + ">" + '<td>'  + response[index].nome + '</td>' + "</a>";							        
						tdHTML = '<td>' + escapeHtml(response[index].nome) + '</td>';
						tdHTML += '<td>' + escapeHtml(response[index].tipoUsuario) + '</td>';
						
					   if (empresa != null){
						   $.each(empresa, function(index, obj){
							    empresas += obj.nome + ".  "
						   });
					   }else{
						   empresas = " - ";
					   }
					   
							   
						tdHTML += '<td>' + escapeHtml(empresas) + '</td>';
						tdHTML += '<td>' + escapeHtml(response[index].email) + '</td>';
						trHTML += '<tr data-id=' + response[index].id + " onclick='buscarUsuario(this)'>"  +  tdHTML +  '</tr>';
						empresas = '';
						
					});
					
					$('#tabelaUsuarios tbody').append(trHTML);
					$(trHTML).show('slow');
				
				pesquisar()
				});
		}
		
		function pesquisar(){
			var $rows = $('#corpoTabela tr');
			$('#input-pesquisa').keyup(function() {
				console.log("muer")
			  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');
	
			  $rows.hide().filter(function() {
			    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			    var matchesSearch = true;
			    $(val).each(function(index, value) {
			      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
			    });
			    return matchesSearch;
			  }).show();
			});
		}
		
		
		function buscarUsuario(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			buscar();
		}
		
		function buscar(){
			
			$.ajax({
				url : localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#formulario-cadastro-usuario #textobotao").css('color', "rgba(255, 255, 255, .7)");
				$('#formulario-cadastro-usuario input').attr('disabled', true);
				$('#formulario-cadastro-usuario select').attr('disabled', true);
				
				$("#formulario-cadastro-usuario input").removeClass("erro");
				
				switch (response.tipoUsuario) {
				case "ANALISTA":
					$('#selectTipoUsuario option[value=2]').prop('selected', true);
					$("#link-perfil-usuario").css('display', 'block');
					break;
				case "COORDENADOR":
					$('#selectTipoUsuario option[value=1]').prop('selected', true);
					$("#link-perfil-usuario").css('display', 'none');
					break;
				case "ADMINISTRADOR":
					$('#selectTipoUsuario option[value=0]').prop('selected', true);
					$("#link-perfil-usuario").css('display', 'none');
					break;
				}
				
				$("#alterar").css("display", "block");
				$("#excluir").css("display", "block");
				$("#senha").css("display", "none");
				$("#lblSenha").css("display", "none");
				
				$("#idUsuario").val(response.id);
				$("#nome").val(response.nome);
				$("#email").val(response.email);
				$("#btnEntrar").attr('disabled', true);
				
				
				$("input:checkbox[class=empresas]").attr('checked', false);
				$.each(response.empresa, function(index){
					
					$("input:checkbox[class=empresas]").each(function() {
						
						if ($(this).val() == response.empresa[index].id) 
							$(this).attr('checked',true)
				    });
				});

				$("#btnEntrar").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"});
				$("#btnEntrar").attr('disabled', true);
				$("#textobotao").text("Confirmar")
				

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
			
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
			$("#container-formulario-cadastro-usuario").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})
		}
		
	</script>

</body>
</html>