<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/gruposolucoes.css" />" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../cadastro/gruposolucao.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	<c:import url="../templates/aviso.jsp" />
	
	<section id="container-grupo">
		<div id="titulo">
			<h1>Grupo de Solução</h1>
		</div>

		<div id="container-pesquisa"> 
			<input	class="pesquisa" id="input-pesquisa" placeholder="Digite a sua pesquisa ..." autofocus="autofocus" > 
		</div>

		<div id="container-tabela-pesquisa">
			<table id="tabelaGrupos" class="table" >
				<thead>
					<tr>
						<th>Código</th>
						<th>Nome</th>
						<th>Data de cadastro</th>
					</tr>
				</thead>
				<tbody id="corpoTabela">
				</tbody>
			</table>
		</div>
	</section>

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
		var tdHTML = '';
		var trHTML = ''; 
		var empresas;
		$(document).ready(function() {
			carregarGrupos();
			
		});
		
		function carregarGrupos(){
			$.ajax({
					url : localIP + "/baseDeConhecimento/grupo",
					dataType : "json",
					method : "GET",
					contentType : "application/json",
					headers : {
					"Authorization" : localStorage.getItem("token")
					},
				}).done(function(response) {
					$('#tabelaGrupos tbody').empty();
					//conta quantos objetos tem no array
					$.each(response,function(index) {
						//devolve as informações de cada um dos objetos encontrados
						//index representa a posicao do objeto... ex: Objeto na psição 2
						// e o atributo desejada
						var empresa = response[index].empresa;
						
						tdHTML = '<td>' + response[index].id + '</td>' + '<td>' + response[index].nome + '</td>' + '<td>' + response[index].dataCadastro + '</td>';
						
						
						/* Listar a empresa com imagem */
						//tdHTML += '<td>' + "<img class='imagemEmpresa' src=" + "http://192.168.2.226/baseDeConhecimento" + response[index].imagem +  ">" + '</td>'; 				
						trHTML = '<tr data-id=' + response[index].id + " onclick='buscarGrupo(this)'>" + tdHTML + '</tr>';
						empresas = '';
						$('#tabelaGrupos tbody').append(trHTML);
					});
					
			});
		}
		$("#input-pesquisa").keyup(function() {
			var rows = $("#corpoTabela").find("tr").hide();
			var data = this.value.split(" ");
			console.log("pesquisa = " + data);
			$.each(data, function(i, v) {
				rows.filter(":contains('" + v + "')").show();
			});
		});
		
		function buscarGrupo(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			buscar();
		}
		
		function buscar(){
			$.ajax({
				url : localIP + "/baseDeConhecimento/grupo/" + localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#idGrupo").val(response.id);
				$("#nome").val(response.nome);
				$('#nome').attr('disabled', true);
				$("#nome").css('background-color','#ccc');
				
				$("#textobotao").text("confirmar")
				$("#alterar").css("display", "block");
				$("#excluir").css("display", "block");

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				
			});
			
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
			$("#container-cadastro-gruposolucao").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})	
		}

	</script>

</body>
</html>