<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/empresas.css" />" rel="stylesheet" />


<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../cadastro/categoria.jsp" />
	<c:import url="../cadastro/uploadexcel.jsp" />
	<c:import url="../cadastro/nivel.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	<c:import url="../templates/aviso.jsp" />
	
	<section id="container-empresas">
		<div id="titulo">
			<h1>Categorias</h1>
		</div>
		
		<div id="container-empresa-e-categoria" >	
		
			<div id="container-categoria-empresa">
				<input class="campos" list="listaEmpresas" id="datalistEmpresas"> 
				<datalist id="listaEmpresas"></datalist>
			
		        <input class="campos" list="empresaCategorias" id="empresaCategoria"> 
				<datalist id="empresaCategorias"></datalist>
				
				<input class="campos" list="empresaCategoriasNiveis1" id="empresaCategoriasNivel1"> 
				<datalist id="empresaCategoriasNiveis1"></datalist>
				
				<input class="campos" list="empresaCategoriasNiveis2" id="empresaCategoriasNivel2"> 
				<datalist id="empresaCategoriasNiveis2"></datalist>
				
				<input class="campos" list="empresaCategoriasNiveis3" id="empresaCategoriasNivel3"> 
				<datalist id="empresaCategoriasNiveis3"></datalist> 
			</div>
				
		</div>
		<div id="container-pesquisa"> 
			<input	class="pesquisa" id="input-pesquisa" placeholder="Digite a sua pesquisa ..." autofocus="autofocus" > 
		</div>
		
		 <label id="container-btnSwitch-categorias-cheias-vazias" for="btnSwitch-categorias-cheias-vazias" >
		 	<input type="checkbox" id='btnSwitch-categorias-cheias-vazias' checked="checked" disabled="disabled">
		 	<div id="divSwitch-categorias-cheias-vazias" class="btnCheias" >
		 	<!-- <input type="button" value="vazios" onclick="categoriaSemProcedimento()"> -->
			</div>
		</label>
			 <span id="msgm-categorias-cheias-vazias">Com prodecimento</span>

		<div id="container-tabela-pesquisa">
			<table id="tabelaCategorias" class="table" >
				<thead>
					<tr>
						<th>Código</th>
						<th>Nome</th>
						<th>Data de cadastro</th>
					</tr>
				</thead>
				<tbody id="corpoTabela">
				</tbody>
			</table>
		</div>
	</section>

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	});
	
	
	//Popular o datalist com as empresas do banco
	var options = '';
	
	$(document).ready(function(){
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){
				
				options =  '<option>' + response[index].nome + '</option>';
				$("#listaEmpresas").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				
			});
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
		
	});
	
		var tdHTML = '';
		var trHTML = ''; 
		var empresas;
		var value = '';
		var empresaID  = '';
		
		/*Popular o datalist de categorias com as categorias da empresa escolhida*/
		$("#datalistEmpresas").on('input', function(){

			$('#corpoTabela').empty();
			$('#empresaCategorias').empty();
			$('#empresaCategoriasNiveis1').empty();
			$('#empresaCategoriasNiveis2').empty();
			$('#empresaCategoriasNiveis3').empty();
			$('#empresaCategoriasNiveis4').empty();
			
			value = $("#datalistEmpresas").val();
			empresaID  = $('#listaEmpresas [value="' + value + '"]').data('id')
			$("#btnSwitch-categorias-cheias-vazias").attr('disabled', false)
			
			
			
		 	if (empresaID != undefined) {
		 		$("#btnSwitch-categorias-cheias-vazias").attr('disabled', false)
		 		
		 		if ($("#btnSwitch-categorias-cheias-vazias").is(':checked')) {
					console.log("checado")
					carregarCategorias();
					
				}else{
					
					console.log("não checado");
					listarCategoriaSemProcedimento();
					
				}	
	
			}else{
				console.log("EMPRESA INVALIDA")
				$("#btnSwitch-categorias-cheias-vazias").attr('disabled', true)
			} 

	});
	
		function carregarCategorias(){

			$('#tabelaCategorias tbody').empty();
			$("#empresaCategorias").empty();
			
			value = $("#datalistEmpresas").val();
			empresaID  = $('#listaEmpresas [value="' + value + '"]').data('id')
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/empresa/" + empresaID ,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				
				//conta quantos objetos tem no array
				$.each(response,function(index) {

					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var categoria = response[index].categoria;
					
					tdHTML = '<td>' + response[index].id + '</td>'+
					'<td>' + response[index].nome + '</td>'+
					'<td>' + response[index].dataCadastro + '</td>';
					
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarCategoria(this)'>" + tdHTML + '</tr>';
					empresas = '';
					
					$('#tabelaCategorias tbody').append(trHTML);
					
					$("#empresaCategorias").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
				
				
				$(trHTML).show('slow');
				
				pesquisar()
				
		}).fail(function(response, textstatus, teste){
			console.log("erro preenchendo as categorias");
		});	
		}
		
		$("#empresaCategoria").on('input', function(){
			
			$('#corpoTabela').empty();
			$('#empresaCategoriasNiveis1').empty();
			$('#empresaCategoriasNiveis2').empty();
			$('#empresaCategoriasNiveis3').empty();
			$('#empresaCategoriasNiveis4').empty();
			
			
			var value = $("#empresaCategoria").val();
			var nivel  = $('#empresaCategorias [value="' + value + '"]').data('id')
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel ,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				
				//conta quantos objetos tem no array
				$.each(response,function(index) {

					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var categoria = response[index].categoria;
					
					tdHTML = '<td>' + response[index].id + '</td>'+
					'<td>' + response[index].nome + '</td>'+
					'<td>' + response[index].dataCadastro + '</td>';
					
					
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarCategoriaNivel(this)'>" + tdHTML + '</tr>';
					empresas = '';
					
					$('#tabelaCategorias tbody').append(trHTML);
					
					$("#empresaCategoriasNiveis1").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
				
				
				$(trHTML).show('slow');
				
				pesquisar()
				
		}).fail(function(response, textstatus, teste){
			console.log("erro preenchendo as categorias");
		});
			
	});
		
		$("#empresaCategoriasNivel1").on('input', function(){
			
			$('#corpoTabela').empty();
			$('#empresaCategoriasNiveis2').empty();
			$('#empresaCategoriasNiveis3').empty();
			$('#empresaCategoriasNiveis4').empty();
			
			var value = $("#empresaCategoriasNivel1").val();
			var nivel  = $('#empresaCategoriasNiveis1 [value="' + value + '"]').data('id')
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel ,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				
				//conta quantos objetos tem no array
				$.each(response,function(index) {

					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var categoria = response[index].categoria;
					
					tdHTML = '<td>' + response[index].id + '</td>'+
					'<td>' + response[index].nome + '</td>'+
					'<td>' + response[index].dataCadastro + '</td>';
					
					
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarCategoriaNivel(this)'>" + tdHTML + '</tr>';
					empresas = '';
					
					$('#tabelaCategorias tbody').append(trHTML);
					
					$("#empresaCategoriasNiveis2").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
				
				
				$(trHTML).show('slow');
				
				pesquisar()
				
		}).fail(function(response, textstatus, teste){
			console.log("erro preenchendo as categorias");
		});
			
	});
		
		$("#empresaCategoriasNivel2").on('input', function(){
			
			$('#corpoTabela').empty();
			$('#empresaCategoriasNiveis3').empty();
			$('#empresaCategoriasNiveis4').empty();
			
			
			var value = $("#empresaCategoriasNivel2").val();
			var nivel  = $('#empresaCategoriasNiveis2 [value="' + value + '"]').data('id')
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel ,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				
				//conta quantos objetos tem no array
				$.each(response,function(index) {

					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var categoria = response[index].categoria;
					
					tdHTML = '<td>' + response[index].id + '</td>'+
					'<td>' + response[index].nome + '</td>'+
					'<td>' + response[index].dataCadastro + '</td>';
					
					
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarCategoriaNivel(this)'>" + tdHTML + '</tr>';
					empresas = '';
					
					$('#tabelaCategorias tbody').append(trHTML);
					
					$("#empresaCategoriasNiveis3").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
				});
				
				
				$(trHTML).show('slow');
				
				pesquisar()
				
		}).fail(function(response, textstatus, teste){
			console.log("erro preenchendo as categorias");
		});
			
	});
		
			
		$("#empresaCategoriasNivel3").on('input', function(){
					
					$('#corpoTabela').empty();
					$('#empresaCategoriasNiveis4').empty();
					
					
					var value = $("#empresaCategoriasNivel3").val();
					var nivel  = $('#empresaCategoriasNiveis3 [value="' + value + '"]').data('id')
					
					$.ajax({
						url: localIP + "/baseDeConhecimento/categoria/niveis/" + nivel ,
						dataType: "Json",
						method: "GET",
						contentType : "application/json",
						headers :{
							"Authorization": localStorage.getItem("token")
						}
					}).done(function(response){
						
						//conta quantos objetos tem no array
						$.each(response,function(index) {
		
							//devolve as informações de cada um dos objetos encontrados
							//index representa a posicao do objeto... ex: Objeto na psição 2
							// e o atributo desejada
							var categoria = response[index].categoria;
							
							tdHTML = '<td>' + response[index].id + '</td>'+
							'<td>' + response[index].nome + '</td>'+
							'<td>' + response[index].dataCadastro + '</td>';
							
							
							trHTML = '<tr data-id=' + response[index].id + " onclick='buscarCategoriaNivel(this)'>" + tdHTML + '</tr>';
							empresas = '';
							
							$('#tabelaCategorias tbody').append(trHTML);
							
							$("#empresaCategoriasNiveis4").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
						});
						
						
						$(trHTML).show('slow');
						
						pesquisar()
						
				}).fail(function(response, textstatus, teste){
					console.log("erro preenchendo as categorias");
				});
					
			});

		
		function pesquisar(){
			var $rows = $('#corpoTabela tr');
			$('#input-pesquisa').keyup(function() {
				console.log("muer")
			  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');
	
			  $rows.hide().filter(function() {
			    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			    var matchesSearch = true;
			    $(val).each(function(index, value) {
			      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
			    });
			    return matchesSearch;
			  }).show();
			});
		}
		
		function buscarCategoria(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			buscarCat();
		}
		
		
		function buscarCat(){
			$.ajax({
				url : localIP + "/baseDeConhecimento/categoria/"+ localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#idCategoria").val(response.id);
				$("#idEmpresa").val(response.empresa.id);
				$("#empresa").val(response.empresa.nome);
				$("#categoria").val(response.nome);
				$('#categoria').attr('disabled', true);
				$('#empresa').attr('disabled', true);
				$("#categoria").css('background-color','#ccc');
								
				$("#textobotao").text("confirmar")
				$("#alterar").css("display", "block");
				$("#excluir").css("display", "block");

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
			
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
			$("#container-cadastro-categoria").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})
		}
		
		function buscarCategoriaNivel(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			buscarNiv();
		}
		
		function buscarNiv(){
			$.ajax({
				url : localIP + "/baseDeConhecimento/categoria/"+ localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				$("#container-cadastro-nivel").css("height", "180px")
				$("#formulario-cadastro-nivel label").css("display", "none");
				$("#formulario-cadastro-nivel input").css("display", "none");
				$("#formulario-cadastro-nivel select").css("display", "none");
				$("#formulario-cadastro-nivel li").css("display", "none");
				$("#container-radio-buttons").css("display", "none");
				
				$("#liNovoNivel").css("display", "block");
				$("#nomeDoNivel").css({"display": "block", "border": "none", "margin-top": "20px", "margin-left": "-40px"});
				$("#idNivel").val(response.id);
				$("#nomeDoNivel").val(response.nome);
								
				$("#formulario-cadastro-nivel #textobotao").text("confirmar")
				

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
			
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
			$("#container-cadastro-nivel").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})
		}
		
		function listarCategoriaSemProcedimento() {
			
			$('#tabelaCategorias tbody').empty();
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/relatorio/empresa/" + empresaID,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				console.log(response.caminhoCategoriasSemProcedimento.length);
				
				$.each(response.caminhoCategoriasSemProcedimento, function(index, value){
					console.log(value)
					
					tdHTML = '<td>' + value + '</td>';
					trHTML = '<tr> ' + tdHTML + '</tr>';
					$('#tabelaCategorias tbody').append(trHTML);	
				});
				
				$(trHTML).show('slow');
				
				pesquisar()
				
			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);

			});
		}
		
	 	$("#btnSwitch-categorias-cheias-vazias").change(function(event){
		    if (this.checked){
		    	$("#container-btnSwitch-categorias-cheias-vazias").css('background-color', '#73bfcb');
		    	 $("#divSwitch-categorias-cheias-vazias").css('margin-left', '0px');
		    	 carregarCategorias(); 
		    	 $("#msgm-categorias-cheias-vazias").text('Com procedimento');
		    	/* $(".niveis-input").attr('disabled', false);
		    	$(".niveis-input").css('background-color', '#fff'); */
		    } else {
		    	$("#divSwitch-categorias-cheias-vazias").css('margin-left', '30px');
		    	$("#container-btnSwitch-categorias-cheias-vazias").css('background-color', 'rgba(220, 220, 220, 1)');
		    	 listarCategoriaSemProcedimento();
		    	$("#msgm-categorias-cheias-vazias").text('Sem procedimento');
		    	/* $(".niveis-input").attr('disabled', true);
		    	$(".niveis-input").css('background-color', 'rgba(220, 220, 220, .4)'); */
		    }
	}); 	
</script>

</body>
</html>