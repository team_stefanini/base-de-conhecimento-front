<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/empresas.css" />" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../cadastro/empresa.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	<c:import url="../templates/aviso.jsp" />
	
	<section id="container-empresas">
		<div id="titulo">
			<h1>Empresas</h1>
		</div>

		<div id="container-pesquisa"> 
			<input	class="pesquisa" id="input-pesquisa" placeholder="Digite a sua pesquisa ..." autofocus="autofocus" > 
		</div>
		
		<label id="container-btnSwitch-empresas-ativos-desativos" for="btnSwitch-empresas-ativos-desativos" >
			 	<input type="checkbox" id='btnSwitch-empresas-ativos-desativos' checked="checked" >
			 	<div id="divSwitch-empresas-ativos-desativos" class="btnAtivados">
			 	</div>
		 	</label>
		 	<span id="msgm-empresas-ativados-desativados">Ativas</span>

		<div id="container-tabela-pesquisa">
			<table id="tabelaEmpresas" class="table" >
				<thead>
					<tr>
						<th>Código</th>
						<th>Nome</th>
						<th>Data de cadastro</th>
					</tr>
				</thead>
				<tbody id="corpoTabela">
				</tbody>
			</table>
		</div>
	</section>
	

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
		
		carregarEmpresasAtivas();
		if(localStorage.getItem("usuario_tipo") !== "ADMINISTRADOR"){
			$("#container-btnSwitch-empresas-ativos-desativos").css("display", "none");
			$("#msgm-empresas-ativados-desativados").css("display", "none");
		}
		
	})
		
		function escapeHtml(unsafe) {
		    return unsafe
		         .replace(/&/g, "&amp;")
		         .replace(/</g, "&lt;")
		         .replace(/>/g, "&gt;")
		         .replace(/"/g, "&quot;")
		         .replace(/'/g, "&#039;");
		 }
		
		/* function carregarEmpresas(){
			$.ajax({
					url : localIP + "/baseDeConhecimento/empresa",
					dataType : "json",
					method : "GET",
					contentType : "application/json",
					headers : {
					"Authorization" : localStorage.getItem("token")
					},
				}).done(function(response) {
					$('#tabelaEmpresas tbody').empty();
					//conta quantos objetos tem no array
					$.each(response,function(index) {
						//devolve as informações de cada um dos objetos encontrados
						//index representa a posicao do objeto... ex: Objeto na psição 2
						// e o atributo desejada
						var empresa = response[index].empresa;
						
						tdHTML = '<td>' + response[index].id + '</td>' + '<td>' + escapeHtml(response[index].nome) + '</td>' + '<td>' + response[index].dataCadastro + '</td>';
						
						
						/* Listar a empresa com imagem *
						//tdHTML += '<td>' + "<img class='imagemEmpresa' src=" + "http://192.168.2.226/baseDeConhecimento" + response[index].imagem +  ">" + '</td>'; 				
						trHTML = '<tr data-id=' + response[index].id + " onclick='buscarEmpresa(this)'>" + tdHTML + '</tr>';
						empresas = '';
						$('#tabelaEmpresas tbody').append(trHTML);
					});
					pesquisar()
			});
		} */
		
		
		function pesquisar(){
			var $rows = $('#corpoTabela tr');
			$('#input-pesquisa').keyup(function() {
				console.log("muer")
			  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');
	
			  $rows.hide().filter(function() {
			    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			    var matchesSearch = true;
			    $(val).each(function(index, value) {
			      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
			    });
			    return matchesSearch;
			  }).show();
			});
		}
		
		function buscarEmpresa(e){
			localStorage.setItem("busca", e.attributes["data-id"].value);
			setTimeout(buscar(), 500);
		}
		
		function buscar(){
			$("#textobotao").text("confirmar");
			$("#upload").css({"box-shadow": "inset 0px 0px 5px #73BFCB", "border-top": "2px solid #73bfcb"});
			$("#container-cadastro-empresa input").removeClass('erro');
			
			$.ajax({
				url : localIP + "/baseDeConhecimento/empresa/" + localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				 $('#descricao-holder').css('display', 'none');
				 $('#imagem-preview').css('display', 'none');
				
				$("#idEmpresa").val(response.id);
				$("#nome").val(response.nome);
				$("#imagemEmpresa").val(response.imagem);
				$('#nome').attr('disabled', true);
				$("#nome").css('background-color','#ccc');
				$('#upload').attr('disabled', true);
				
				$("#saibaMais").css("display", "block");
				$("#textobotao").text("confirmar")
				$("#alterar").css("display", "block");
				$("#excluir").css("display", "block");

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
			
			$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "9"})
			$("#container-cadastro-empresa").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "10"})
		}
		
		$("#btnSwitch-empresas-ativos-desativos").change(function(event){
		    if (this.checked){
		    	$("#container-btnSwitch-empresas-ativos-desativos").css('background-color', '#73bfcb')
		    	 $("#divSwitch-empresas-ativos-desativos").css('margin-left', '0px')
		    	 carregarEmpresasAtivas();
		    	 $("#msgm-empresas-ativados-desativados").text('Ativas');
		    } else {
		    	$("#divSwitch-empresas-ativos-desativos").css('margin-left', '30px')
		    	$("#container-btnSwitch-empresas-ativos-desativos").css('background-color', 'rgba(220, 220, 220, 1)')
		        carregarEmpresasInativas();
		    	$("#msgm-empresas-ativados-desativados").text('Inativas');
		    }
	}); 	
	 	
	 function carregarEmpresasAtivas(){
		 $('#tabelaEmpresas tbody').slideUp('fast');
		 
			var tdHTML = '';
			var trHTML = ''; 
			var empresas;
			
			$.ajax({
				url : localIP + "/baseDeConhecimento/empresa",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers : {
				"Authorization" : localStorage.getItem("token")
				},
				
			}).done(function(response) {
				$('#tabelaEmpresas tbody').empty();
				//conta quantos objetos tem no array
				$.each(response,function(index) {
					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var empresa = response[index].empresa;
					
					tdHTML = '<td>' + response[index].id + '</td>' + '<td>' + escapeHtml(response[index].nome) + '</td>' + '<td>' + response[index].dataCadastro + '</td>';
					
					
					/* Listar a empresa com imagem */
					//tdHTML += '<td>' + "<img class='imagemEmpresa' src=" + "http://192.168.2.226/baseDeConhecimento" + response[index].imagem +  ">" + '</td>'; 				
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarEmpresa(this)'>" + tdHTML + '</tr>';
					empresas = '';
					$('#tabelaEmpresas tbody').append(trHTML).slideDown();
				});
				pesquisar()
		});
	 }
	 
	function carregarEmpresasInativas(){
		$('#tabelaEmpresas tbody').slideUp('fast');
		
			var tdHTML = '';
			var trHTML = ''; 
			var empresas;
		
			$.ajax({
				url : localIP + "/baseDeConhecimento/empresa/inativas",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers : {
				"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response) {
				$('#tabelaEmpresas tbody').empty();
				//conta quantos objetos tem no array
				$.each(response,function(index) {
					//devolve as informações de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psição 2
					// e o atributo desejada
					var empresa = response[index].empresa;
					
					tdHTML = '<td>' + response[index].id + '</td>' + '<td>' + escapeHtml(response[index].nome) + '</td>' + '<td>' + response[index].dataCadastro + '</td>';
					
					
					/* Listar a empresa com imagem */
					//tdHTML += '<td>' + "<img class='imagemEmpresa' src=" + "http://192.168.2.226/baseDeConhecimento" + response[index].imagem +  ">" + '</td>'; 				
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarEmpresa(this)'>" + tdHTML + '</tr>';
					empresas = '';
					$('#tabelaEmpresas tbody').append(trHTML).slideDown();
				});
				pesquisar()
			});
		}
		
	</script>
</body>
</html>