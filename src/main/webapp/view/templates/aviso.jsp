<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/aviso.css" />">

<div id="container-avisos-cadastro">
</div>

<div id="container-aviso-cadastrado">
	<ul>
		<li class="li-notificacao-cadastrada">
			<p id="titulo"></p>
			<p id="descricao"></p>
			<p id="aviso-data"></p>
		</li>
	</ul>
</div>

<div id="container-quantidade-avisos"></div>
	<div id="container-avisos">
		<div id="avisos"></div>
		<div id="container-lista-notificacoes">
			<ul>

			</ul>
		</div>
	</div>	

	<script type="text/javascript">
	
	
	$(document).ready(function(){
		quantidadeDeAvisos();
	});
	
	
	function quantidadeDeAvisos(){
 			$.ajax({
				url: localIP + "/baseDeConhecimento/avisos/usuario",
				dataType: "json",
				method: "GET",
				contentType: "application/json",
				headers : {
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste){			
				
				console.log(response)
				
				if(response == 0){
					
					$("#container-quantidade-avisos").css('display', 'none');
				}else{
					
					$("#container-quantidade-avisos").css('display', 'flex');
					$("#container-quantidade-avisos").text(response)
				}
				
			}).fail(function(response, textstatus, teste){
				
				console.log(response.status);
				
			}); 
	}
	
	var i = 0;
		$("#avisos").click(function(){
			
			$(this).toggleClass("avisosAtivo")

			if(!i){
			 i = 1;
				 $("#container-avisos").css("right", "0px");
				 $("#container-avisos-cadastro").css('z-index', '5');
			} else {
			 i = 0;
			 	$("#container-avisos").css("right", "-345px");
			 	
			 	$("#container-avisos-cadastro").css({'opacity': '0', 'z-index': '', 'top': '23.5%', 'left': '100%'});
				setTimeout(function(){
					$("#container-avisos-cadastro").css('display', '');
				}, 1000);
			}
			
			$("#container-quantidade-avisos").css("display", "none");
			
			 $.ajax({
				url: localIP + "/baseDeConhecimento/aviso",
				dataType: "json",
				method: "GET",
				contentType : "application/json",
				headers : {
					"Authorization" : localStorage.getItem("token")
					}
			}).done(function(response){
				$("#container-lista-notificacoes").empty()
				var ul = document.createElement('ul');	
				
				$("#container-avisos-cadastro").empty();
				
				if (localStorage.getItem('usuario_tipo') != "ANALISTA") {
					/* var divContainerCadastroAviso = document.createElement('div')
					$(divContainerCadastroAviso).attr('id', 'container-avisos-cadastro'); */
					var divAvisoTituloDescricao = document.createElement('div');
					$(divAvisoTituloDescricao).attr('id', 'divAvisoTituloDescrica');
					
					var divAvisoEmpresasBtnCadastrar = document.createElement('div');
					$(divAvisoEmpresasBtnCadastrar).attr('id', 'divAvisoEmpresasBtnCadastrar');
					
					var divAvisobtnVoltabtnCadastrar = document.createElement('div');
					$(divAvisobtnVoltabtnCadastrar).attr('id', 'divAvisobtnVoltabtnCadastrar');
					
					var btnAvisoTela2 = document.createElement('button');
					$(btnAvisoTela2).attr('id', 'btnAvisoTela2');
					$(btnAvisoTela2).attr('onClick', 'chamarTela2Aviso()');
					
					var btnAvisoVoltarTela1 = document.createElement('button');
					$(btnAvisoVoltarTela1).attr('id', 'btnAvisoVoltarTela1');
					$(btnAvisoVoltarTela1).attr('onClick', 'chamarTela1Aviso()');
					
					var inputAvisoTitulo = document.createElement('input');
					$(inputAvisoTitulo).attr('id', 'cadastrarAvisoTitulo');
					$(inputAvisoTitulo).attr('placeholder', 'titulo');
					
					var textareaAvisoDescricao = document.createElement('textarea');
					$(textareaAvisoDescricao).attr('id', 'cadastrarAvisoDescricao');
					$(textareaAvisoDescricao).attr('placeholder', 'Descrição');
					
					var inputPesquisarEmpresa = document.createElement('input')
					$(inputPesquisarEmpresa).attr('id', 'pesquisar-empresa-aviso');
					
					var ulListaEmpresasAviso = document.createElement('ul');
					$(ulListaEmpresasAviso).attr('id', 'lista-empresas-aviso');
					
					var btnCadastrarAviso = document.createElement('button');
					$(btnCadastrarAviso).attr('id', 'btnCadastrarAviso');
					$(btnCadastrarAviso).attr('onClick', 'cadastrarAviso()')
					$(btnCadastrarAviso).attr('title', 'enviar');
					
					$(divAvisoTituloDescricao).append(inputAvisoTitulo);
					$(divAvisoTituloDescricao).append(textareaAvisoDescricao);
					$(divAvisoTituloDescricao).append(btnAvisoTela2);
					
					$(divAvisobtnVoltabtnCadastrar).append(btnAvisoVoltarTela1);
					$(divAvisobtnVoltabtnCadastrar).append(btnCadastrarAviso);
					
					$(divAvisoEmpresasBtnCadastrar).append(inputPesquisarEmpresa);
					$(divAvisoEmpresasBtnCadastrar).append(ulListaEmpresasAviso);
					$(divAvisoEmpresasBtnCadastrar).append(divAvisobtnVoltabtnCadastrar);
					
					$("#container-avisos-cadastro").append(divAvisoTituloDescricao);
					$("#container-avisos-cadastro").append(divAvisoEmpresasBtnCadastrar);
					
					var chamarCadastroAviso = document.createElement('button');
					$(chamarCadastroAviso).attr('id', 'chamarCadastroAviso');
					$(chamarCadastroAviso).text("Cadastrar novo aviso")
					$(chamarCadastroAviso).attr('onClick', 'chamarCadastroAviso()')
					
					var liCadastrarAviso = document.createElement('li');
					$(liCadastrarAviso).addClass('li-notificacao');
					$(liCadastrarAviso).attr('id', 'liCadastrarAviso')
					$(liCadastrarAviso).append(chamarCadastroAviso);
					
					$(ul).append($(liCadastrarAviso))
				}
				
				$.each(response, function(index){
					
					var li = document.createElement('li');
					$(li).addClass('li-notificacao');
					$(li).attr('id', response[index].id)
					
					var btnExcluirAviso = document.createElement('button');
					$(btnExcluirAviso).addClass('btnExcluirAviso');
					$(btnExcluirAviso).attr('onClick', 'excluirAviso(this)')
					
					var pTitulo = document.createElement('p');
					$(pTitulo).addClass('titulo');
					$(pTitulo).text(response[index].titulo);
					
					var pDescricao = document.createElement('p');
					$(pDescricao).addClass('descricao');
					$(pDescricao).text(response[index].descricao);
					
					var pAvisoData = document.createElement('p');
					$(pAvisoData).addClass('aviso-data');
					$(pAvisoData).text(response[index].dataCadastro);
					
					$(li).append(pTitulo);
					$(li).append(pDescricao);
					$(li).append(pAvisoData);
					
					if (localStorage.getItem("usuario_tipo") != "ANALISTA") 
						$(pAvisoData).append(btnExcluirAviso);
					
					$(ul).append($(li)); 
				});
				
				$("#container-lista-notificacoes").append(ul);
				
				$("#container-lista-notificacoes ul .li-notificacao").each(function(index){
					$(this).delay(index*100).toggle("slide");
					
				});
				
				pesquisarEmpresasAviso();
			}).fail(function(response, textstatus, teste){
				console.log(teste.status);
				console.log(response.status);
				console.log(textstatus);
			}); 
			
		});
		
		function cadastrarAviso(){
		
			var empresaObj = [];
			
			$("input:checkbox[class=empresas-aviso]:checked").each(function() {
		        var id = $(this).val();
		        item = {}
		        item ["id"] = id;

		        empresaObj.push(item);
		    });
			
			var avisoObj = {
				"titulo" : $("#cadastrarAvisoTitulo").val(),
				"descricao" : $("#cadastrarAvisoDescricao").val(),
				"empresa" : empresaObj
			}
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/aviso",
				dataType: "json",
				method: "POST",
				contentType: "application/json",
				data: JSON.stringify(avisoObj),
				headers : {
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste){			
				
				$("#container-avisos-cadastro").css({'opacity': '0', 'z-index': '-15'});
				/* $("#container-avisos-cadastro").css('display', 'none'); */
				
				$("#container-aviso-cadastrado ul li #titulo").text(response.titulo)
				$("#container-aviso-cadastrado ul li #descricao").text(response.descricao)
				$("#container-aviso-cadastrado ul li #aviso-data").text(response.dataCadastro)
				
				$("#container-aviso-cadastrado").css({'opacity': '1', 'z-index': '20'});
				
				var linhaNova = $("#container-aviso-cadastrado ul li").clone();
				$("#container-lista-notificacoes ul li:first-child").after(linhaNova);
				
				$("#container-aviso-cadastrado").css({'opacity': '0', 'z-index': '-20'});
				
				linhaNova.hide().show('slow')
				
			}).fail(function(response, textstatus, teste){
				
				console.log(response);
				console.log(response.status);
				console.log("Textstatus = " + textstatus);
			}); 		
		}
		
		function excluirAviso(botao){
			
				avisoID = $(botao).closest("li").attr("id");
				
				$.ajax({
					url: localIP + "/baseDeConhecimento/aviso/" + avisoID,
					dataType: "json",
					method: "DELETE",
					contentType: "application/json",
					headers : {
						"Authorization" : localStorage.getItem("token")
					},
				}).done(function(response, textstatus, teste){
				
					$(botao).closest("li").slideUp();
					
				}).fail(function(response, textstatus, teste){
					
				  console.log(response);
				  console.log(response.status);
				  console.log("Textstatus = " + textstatus);
				
				});
		}
		
		function chamarCadastroAviso(){
			
			$("#cadastrarAvisoTitulo").val('');
			$("#cadastrarAvisoDescricao").val('');
			
			$("#container-avisos-cadastro").css('display', 'flex');
			setTimeout(function(){
				$("#container-avisos-cadastro").css({'top': '50%', 'left': '50%', 'opacity': '1', 'z-index': '1'});
			}, 200)
			
			$("#divAvisoTituloDescrica").css({'z-index': '10', 'display': 'flex'});
			$("#divAvisoEmpresasBtnCadastrar").css({'z-index': '10', 'display': 'none'})
		}
		
		function pesquisarEmpresasAviso(){
			$('#pesquisar-empresa-aviso').keyup(function() {
				  var $rows = $('#lista-empresas-aviso li');	
				  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');
		
				  $rows.hide().filter(function() {
				    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				    var matchesSearch = true;
				    $(val).each(function(index, value) {
				      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
				    });
				    return matchesSearch;
				  }).show();
			});
		}
		
		function chamarTela2Aviso(){
			 $("#lista-empresas-aviso").empty();
				$.ajax({
				url: localIP + "/baseDeConhecimento/empresa",
				dataType: "json",
				method: "GET",
				contentType : "application/json",
				headers : {
					"Authorization" : localStorage.getItem("token")
					}
			}).done(function(response){
				
				/*  $("#lista-empresas-aviso").append("<li class='liempresas'>" + "<input id='selecionarTodasEmpresasAviso' class='empresas-aviso' type='checkbox' >"  + 'Selecionar todas' + "</li>"); */
				$("#lista-empresas-aviso").append("<li class='liempresas'>" + "<input class='empresas-aviso-selecionar-todas'  id='empresas-aviso-selecionar-todas' type='checkbox' >" + 'Selecionar todas empresas' + "</li>");
				selecionarTodasEmpresasAviso()
				
				$.each(response, function(index){

					options =  '<option>' + response[index].nome + '</option>';
					
					
					 $("#lista-empresas-aviso").append("<li class='liempresas'>" + "<input class='empresas-aviso' type='checkbox' value=" + response[index].id + ">" + response[index].nome + "</li>"); 
				
				});
				/* excluirAviso() */
				
				
			}).fail(function(response, textstatus, teste){
				console.log(teste.status);
				console.log(response.status);
				console.log(textstatus);
			}); 
			
			$("#divAvisoTituloDescrica").css({'z-index': '-15', 'display': 'none'})
			$("#divAvisoEmpresasBtnCadastrar").css({'z-index': '15', 'display': 'flex'})
		}
		
		function chamarTela1Aviso(){
			$("#divAvisoTituloDescrica").css({'z-index': '15', 'display': 'flex'})
			$("#divAvisoEmpresasBtnCadastrar").css({'z-index': '-15', 'display': 'none'})
		}
		
		function selecionarTodasEmpresasAviso(){
			$('#empresas-aviso-selecionar-todas').change(function(){
				
				if ($(this).prop('checked')) {
					$(".empresas-aviso").prop('checked', true);
				}else{
					$(".empresas-aviso").prop('checked', false);
				}
				
			});	
		}
				
	</script>
	