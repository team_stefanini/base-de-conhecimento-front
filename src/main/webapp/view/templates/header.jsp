<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="/resources/imagens/icone_menu.png" var="raiz" />


<link type="text/css" href="<c:url value="/resources/css/header.css" />" rel="stylesheet" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/config.js" />"></script>

<header id="cabecalho">
	
		<ul>
			<li id="azul" ><img src="<c:url value="/resources/imagens/quase3.png"/>"> <span class="seta" id="primeiraSeta"></span><span class="legenda" id="simbolo1"> Perfil </span></li>
			<a href="../analista/analista.jsp"><li id="linkAnalista" ><img class="iconesmenu" src="<c:url value="/resources/imagens/home.png"/>"> <span class="seta"></span><span class="legenda" > Inicio  </span> </li></a>
			<a href="../visualizar/relatorio.jsp"><li><img class="iconesmenu" src="<c:url value="/resources/imagens/simbolo4.png"/>"> <span class="seta"></span> <span class="legenda" > Relat�rios </span> </li></a>
			<a href="../lista/usuarios.jsp"><li><img class="iconesmenu" src="<c:url value="/resources/imagens/simbolo3.png"/>"> <span class="seta"></span><span class="legenda" > Usuarios </span> </li></a>
			<a href="../lista/categorias.jsp"><li><img  class="iconesmenu" src="<c:url value="/resources/imagens/categoria.png"/>"> <span class="seta"></span><span class="legenda" > Categorias </span> </li></a>
			<a id="linkAdm" href="../lista/empresas.jsp"><li id="linkAdministrador"><img class="iconesmenu" src="<c:url value="/resources/imagens/simbolo5.png"/>"> <span class="seta"></span><span class="legenda" > Empresas  </span> </li></a>
			<a href="../lista/gruposolucoes.jsp"><li><img class="iconesmenu" src="<c:url value="/resources/imagens/grupos1.png"/>"> <span class="seta"></span><span class="legenda" >Gps. de Solu��o  </span> </li></a>
			<a href="../cadastro/procedimento.jsp"><li><img class="iconesmenu" src="<c:url value="/resources/imagens/procedimento.png"/>"> <span class="seta"></span><span class="legenda" > Procedimento </span> </li></a>
			<a href="../../index.jsp" id="sair"><li id="liSAIR"><img class="iconesmenu" src="<c:url value="/resources/imagens/sair.png"/>"> <span class="seta"></span><span class="legenda" > Sair </span> </li></a>
		</ul>

</header>

<script>


/* Carrega os itens do menu um de cada vez*/

$(document).ready(function(){
	if (localStorage.getItem("usuario_tipo") == "ANALISTA") {
		$("#cabecalho ul").css('justify-content', 'flex-start')
		$("#linkAnalista").css('margin-top', '30px')
		$("#cabecalho ul a").css('margin-bottom', '10px')
		$("#azul").delay(50).fadeIn();
		$("#linkAnalista").delay(100).fadeIn();
		$("#liSAIR").delay(150).fadeIn();

	}else if (localStorage.getItem("usuario_tipo") == "COORDENADOR"){
		$("#cabecalho li").each(function(index){
			if ($(this).attr('id') == 'linkAdministrador') {
				$('#linkAdm').remove();
			}else{
			 	$(this).delay(index*50).fadeIn();
			}
	   	});		
	}else{
		$("#cabecalho li").each(function(index){
			 $(this).delay(index*50).fadeIn();   
	   	});
	}
});


$("#sair").click(function(){
	localStorage.clear();
	$(location).attr("href" , "../../index.jsp" );
})


</script>