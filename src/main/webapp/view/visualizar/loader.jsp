<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/loader.css" />" rel="stylesheet" />

<div>
  <dt></dt>
  <dd></dd>
</div>

	<script type="text/javascript">
	$(document).ajaxStart(function() {
		//only add progress bar if added yet.
		  if ($("#progress").length === 0) {
		    $("body").append($("<div><dt/><dd/></div>").attr("id", "progress"));
		    $("#progress").width((50 + Math.random() * 30) + "%");
		  }
		});

		$(document).ajaxComplete(function() {
		//End loading animation
		    $("#progress").width("101%").delay(200).fadeOut(400, function() {
		      $(this).remove();
		    });
		});
	
	</script>

</html>