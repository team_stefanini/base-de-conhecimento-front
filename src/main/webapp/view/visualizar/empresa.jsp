<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css"
	href="<c:url value="/resources/css/empresa.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />

	
	<section id="container-cadastro-empresa" >
		<form id="formulario" enctype="multipart/form-data">
			<div id="titulo">
				<h1>Empresa</h1>
			</div>
			<label>Empresa</label>
			<input type="text" id="nome" name="nome" autofocus="autofocus" disabled="disabled">
			
			<label id="selecionarArquivo">Selecionar arquivo</label>
			<input type="file" id="inputFile" name="file" disabled="disabled">
			
			<!-- <div id="image-holder"></div>  -->
			
			<button  onclick="criarEmpresa()" id="upload" ><span id="textobotao">Criar Empresa</span> </button> 
		</form>
	</section>	

	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	
	/* Buscar empresa */
	$(document).ready(function(){
		buscarEmpresa();
	});
	
		function buscarEmpresa() {
			var formData = new FormData();
			var inputFile = document.getElementById("inputFile");					
			
			formData.append('nome', $("#nome").val());
			formData.append('file', inputFile.files[0]);

			console.log("ENTREI");

			$.ajax({
				url : localIP + "/baseDeConhecimento/empresa/" + localStorage.getItem("busca"),
				type : 'GET',
				data : formData,
				processData : false,
				contentType : false,
				cache : false,				
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				$("#nome").val(response.nome);
				console.log(response)
				$("#textobotao").text("sucessso")
				$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
		}
		
		$('#selecionarArquivo').on('click', function() {
			  $('#inputFile').trigger('click');
			})
	</script>
</body>
</html>