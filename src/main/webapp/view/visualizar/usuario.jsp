<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/index.css" />"	rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/usuario.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />

	<div id="container-formulario-cadastro-usuario">
		<form action="" id="formulario-cadastro-usuario">
			<div id="titulo">
				<h1>Cadastro de Usuario</h1>
			</div>
		
			<label>Nome</label>
			<input type="text" autofocus id="nome">
			<label>Usuario</label>
			<input type="email" id="email">
			<label>Usuario</label>
			<input type="email">
			<label>Senha</label>
			<input type="password" id="senha">
			
			<div id="tipos-usuario" >
			
			<label>Tipo de Usuario</label>
			<input type="radio" name="tipoUsuario" value="2" >Analista
			<input type="radio" name="tipoUsuario" value="1" >Cordernador
			<input type="radio" name="tipoUsuario" value="0" >Administrador
			<label>Empresa</label>
			
			</div>
			<div id="quantidade-empresas" style="display: none">
			
			<input type="radio" name="quantidadeEmpresa" value="1"> 1<br>
			<input type="radio" name="quantidadeEmpresa" value="2"> 2<br>
			<input type="radio" name="quantidadeEmpresa" value="3"> 3
			
			<input class="empresas" list="empresas1" id="empresa1"  style="display: none">
			<datalist id="empresas1" >
			
			</datalist>
			
			<input class="empresas" list="empresas2" id="empresa2" style="display: none">
			<datalist id="empresas2" >

			</datalist>
			
			<input class="empresas" list="empresas3" id="empresa3" style="display: none">
			<datalist id="empresas3" >

			</datalist>
			
			</div>
			<button id="btnEntrar" type="button" onclick="criarUsuario()">Entrar</button>
		</form>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	var quantidadeEmpresa = 0;
	var tipoUsuario = 0;
	
	$('document').ready(function(){
             buscarUsuario();   
    });
	
	
	function buscarUsuario(){
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("busca"),
			dataType: "json",
			method: "GET",
			contentType: "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			
			$("#nome").val(response.nome)
			
		}).fail(function(response, textstatus, teste){
			console.log("teste = " + teste.status);
			console.log("response = " + response);
			console.log("CODIGO = " + textstatus);
		});
		
	}
	
	
	</script>

</body>
</html>