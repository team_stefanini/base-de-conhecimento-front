<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css"  href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css"  href="<c:url value="/resources/css/visualizarprocedimento.css" />" rel="stylesheet" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>

<title>Stefanini</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	
	<div id="container-visualizar-procedimento">
		<div id="conteudo-procedimento">
				<h1 id="tituloProcedimento"></h1>
				<div id="procedimento-descricao">
					<ul id="ul-visualizar-procedimento">	
						<li class="borda-lateral">Código: <span id="idProcedimento"></span></li>
						<li class="borda-lateral">Elegivel: <span id="elegivel"></span> </li>
						<li class="borda-lateral">Versão: <span id="versao"></span> </li>
						<li class="borda-lateral">Grupo Solução: <span id="grupoSolucao"></span> </li>
						<li id="sem-borda">Autor: <span id="usuarioNome"></span> </li>
					</ul>
				</div>
				
				<p id="descricao"></p>
		</div>
			<input type="button" onclick="aprovarProcedimento()" id="aprovarProcedimento" title="Aprovar procedimento">	
			<input type="button" onclick="alterarProcedimento()" id="alterarProcedimento" title="Alterar Procedimento">
			<input type="button"  id="btnPdf" title="Gerar PDF">
			<input type="button" onclick="confirmar()" id="excluirConfirmar" title="Excluir Procedimento"> 				
	</div>
	
	<div id="modal-confirmar">
		<h2>Confirmação </h2>
					
		<span id="mensagemDeConfirmacao"> Tem certeza de que deseja excluir o procedimento, <span id="idDoProcedimento"></span> ? </span>
					
		<div id="mensagemDeConfirmacaoBotoes">
			<input type="button" id="btnSim"  value="SIM" onclick="excluirProcedimento()">
			<input type="button" id="btnNao"  value="NÃO" onclick="fecharModal()">
		</div>
	</div>
	
	<div id="totalModal"></div>
	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == null) {
			$(window.location.href = "../../index.jsp").fadeIn('500');
		}
	})
	
	
	$('#btnPdf').click(function () {
		$(window.location.href = "pdf.jsp").fadeIn('500');
	})
		
	
	var idProcedimento = '';
	
	/* buscar um  procedimento */

	
	function alterarProcedimento(){
		localStorage.setItem("editar_procedimento_id", idProcedimento);
		$(window.location.href="../cadastro/procedimento.jsp");
	}
	
	
	function fecharModal(){
		$("#totalModal").css({"opacity": '', "z-index": ''});
		$("#modal-confirmar").css({"opacity": '', "z-index": '', "animation": '', "animation-fill-mode": ''});
	}
	
	/* mostra a modal para confirmar a exclusao do procedimento*/
	function confirmar(){
		$("#idDoProcedimento").text(idProcedimento);
		$("#totalModal").css({"opacity": "1", "z-index": "10"});
		$("#modal-confirmar").css({"opacity": "1", "z-index": "11", "animation": "movimento .2s", "animation-fill-mode": "forwards"});
	}
	
	/*Fecha a modal se o usuario clicar fora dela*/
	$("#totalModal").click(function(){
		$("#totalModal").css({"opacity": '', "z-index": ''});
		$("#modal-confirmar").css({"opacity": '', "z-index": '', "animation": '', "animation-fill-mode": ''});
	});
	
	$(document).ready(function(){
		buscarProcedimento();
		
		if (localStorage.getItem("usuario_tipo") == "ANALISTA") {
			$("#alterarProcedimento").css("display", "none");
			$("#excluirConfirmar").css("display", "none");
			$("#btnPdf").css("display", "none");
		}else{
			
		}
		
	});
	
	function excluirProcedimento(){
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/procedimento/" + idProcedimento,
			dataType: "json",
			method: "DELETE",
			contentType: "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
			}
		}).done(function(response, textstatus, teste){
			$("#mensagemDeConfirmacao").fadeOut(100);
			$("#btnSim").fadeOut(100);
			$("#btnNao").fadeOut(100);
			setTimeout(function(){
				$("#mensagemDeConfirmacao").fadeIn().text("O procedimento " + idProcedimento + " foi excluido com sucesso");
			}, 200);
			
		}).fail(function(response, textstatus, teste){
			
			$("#mensagemDeConfirmacao").text("O Procedimento, " + idProcedimento + " não pode ser excluido");
			
		});
	}
	
	/* Titulo procedimento para o pdf  */
	var tituloProcedimentoPdf = '';
	var empresaProcedimentoPdf ='';
	
	function buscarProcedimento(){
		$.ajax({
			url : localIP + "/baseDeConhecimento/procedimento/" + localStorage.getItem("busca"),
			type : 'GET',	
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste) {

			 carregarInformacoesProcedimento(response);
			
		}).fail(function(response, textstatus, teste) {
			
		});
	}
	
	function  carregarInformacoesProcedimento(procedimentoOBJ){
		
		if (procedimentoOBJ.status == true) {
			$('#aprovarProcedimento').css('display', 'none');
		} else{
			$('#aprovarProcedimento').css('display', 'block');
		}
		
		
		$("#idProcedimento").text(procedimentoOBJ.id);
		$("#tituloProcedimento").text(procedimentoOBJ.titulo);
		tituloProcedimentoPdf = procedimentoOBJ.titulo;
		
		$("#descricao").append(procedimentoOBJ.descricao);
		
		$("#versao").append(procedimentoOBJ.versao);
		
		if (procedimentoOBJ.grupoSolucao != null ) {
			$("#ul-visualizar-procedimento li #grupoSolucao").append(procedimentoOBJ.grupoSolucao.nome);
		}else{
			$("#ul-visualizar-procedimento li:nth-child(4)").css("display", "none");
		}
		
		$("#usuarioNome").append(procedimentoOBJ.usuario.nome);
		empresaProcedimentoPdf = procedimentoOBJ.categoria.empresa.nome;
		
		if (procedimentoOBJ.elegivel == true) {
			$("#ul-visualizar-procedimento #elegivel").text("Sim");
		}else{
			$("#ul-visualizar-procedimento #elegivel").text("Não");
		}
		
/* 		if (procedimentoOBJ.grupoSolucao == null) {
			console.log("aaaa")
		}else{
			console.log("vvvv")
		} */
		
		idProcedimento = $("#idProcedimento").text();
	}
	
	function aprovarProcedimento(){
		
		var procedimentoObj = {
			"id": localStorage.getItem('busca')
		}
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/procedimento/aprovacao",
			dataType: "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify(procedimentoObj),
			headers : {
				"Authorization" : localStorage.getItem("token")
			}
		}).done(function(response, textstatus, teste){
			console.log("ativado")
			
		}).fail(function(response, textstatus, teste){
			
			console.log(response.status)	
			console.log(textstatus)
			console.log(teste)
			
			
		});
	}
	
	</script>
</body>
</html>