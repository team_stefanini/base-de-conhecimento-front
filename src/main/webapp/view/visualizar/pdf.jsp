<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>

<link type="text/css"  href="<c:url value="/resources/css/pdf.css" />" rel="stylesheet" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/config.js" />"></script>


<title>Stefanini</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<div id="container-visualizar-procedimento">
		<div id="conteudo-procedimento">
				<h1 id="tituloProcedimento"></h1>
				<div id="procedimento-descricao">
					<ul id="ul-visualizar-procedimento">	
						<li class="borda-lateral">Código: <span id="idProcedimento"></span></li>
						<li class="borda-lateral">Elegivel: <span id="elegivel"></span> </li>
						<li class="borda-lateral">Versão: <span id="versao"></span> </li>
						<li class="borda-lateral">Grupo Solução: <span id="grupoSolucao"></span> </li>
						<li id="sem-borda">Autor: <span id="usuarioNome"></span> </li>
					</ul>
				</div>
				
				<p id="descricao"></p>
		</div>	
	</div>
	<input type="button"  id="btnPdf" title="Gerar PDF">
	
	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == null) {
			$(window.location.href = "../../index.jsp").fadeIn('500');
		}else{
			$.ajax({
				url : localIP + "/baseDeConhecimento/procedimento/" + localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				console.log(JSON.stringify(response))
				 carregarInformacoesProcedimento(response);
				 
			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
			});
		}
	})
	
	
	/* Titulo procedimento para o pdf  */
	var tituloProcedimentoPdf = '';
	var empresaProcedimentoPdf ='';
	
	function  carregarInformacoesProcedimento(procedimentoOBJ){
		$("#idProcedimento").text(procedimentoOBJ.id);
		$("#tituloProcedimento").text(procedimentoOBJ.titulo);
		tituloProcedimentoPdf = procedimentoOBJ.titulo;
		$("#descricao").append(procedimentoOBJ.descricao);
		$("#versao").append(procedimentoOBJ.versao);
		
		if (procedimentoOBJ.grupoSolucao != null) {
			idGrupoSolucao = procedimentoOBJ.grupoSolucao.id;
			$("#grupoSolucao").val(procedimentoOBJ.grupoSolucao.nome)
	 	}else{
	 		$("#grupoSolucao").val('');
	 	}
		$("#usuarioNome").append(procedimentoOBJ.usuario.nome);
		empresaProcedimentoPdf = procedimentoOBJ.categoria.empresa.nome;
		
		if (procedimentoOBJ.elegivel == true) {
			$("#elegivel").text("Sim");
		}else{
			$("#elegivel").text("Não");
		}
		
		if (procedimentoOBJ.grupoSolucao == null) {
			
		}else{
			
		}
		
		idProcedimento = $("#idProcedimento").text();
	}
	$('#btnPdf').click(function () {
		window.print();
	})
	
	</script>
</body>
</html>