<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/perfilusuario.css" />" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:import url="../templates/header.jsp"></c:import>
	<c:import url="../visualizar/perfil.jsp"></c:import>
	
	<div id="container-perfil-usuario">
		<div id="container-perfil-usuario-imagem-informacoes">
		
			<div id="container-perfil-usuario-imagem">
				<img alt="" src="">
			</div>
			<div id="container-perfil-usuario-informacoes">
				<ul>
					<li>Nome: <span  class="info" id="nome"></span><span class="info" id="tipo">analista</span> </li>
					<li>Empresas: <span class="info" id="empresas"></span></li>
					<li>Data de Cadastro: <span class="info" id="data-de-cadastro"></span> </li>
					<li>Status: <span class="info" id="status"></span></li>
				</ul>
			</div>
			
		</div>
		
		<div id="container-perfil-usuario-tabela">
			<h1>Procedimentos mais acessados</h1>
			<div id="container-perfil-usuario-tabela-empresa-e-pesquisa" >
				<input type="text" list="data-list-empresas" id="lista-empresas-do-usuario">
				
				<datalist id="data-list-empresas">
				</datalist>
				
				<input type="text" id="input-pesquisar-procedimento-mais-acessado">
			</div>
			<div id="container-tabela-procedimentos-mais-acessados">
				<table id="tabela-procedimentos-mais-acessados">
					<thead>	
						<tr>
							<td>ID</td>
							<td>Titulo</td>
							<td>Quantidade de acessos</td> 
						</tr>
					</thead>
					<tbody id="tabela-procedimentos-mais-acessados-corpo">
						
					</tbody>
				</table>
			</div>
		</div>	
	</div>
	
	<script type="text/javascript">
	var tdHTML = '';
	var trHTML = ''; 
	
	$(document).ready(function(){
		buscarInformacoesDoUsuario()
		carregarTabelaDeProcedimentos()
	});

	
	function buscarInformacoesDoUsuario(){
			
			$.ajax({
				url : localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("busca"),
				type : 'GET',	
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				$("#container-perfil-usuario-informacoes #nome").text(response.nome);
				
				$.each(response.empresa, function(index){
						console.log("Mateus " + response)
					
					 $("#container-perfil-usuario-informacoes #empresas").append(response.empresa[index].nome + ". "); 
					 
					 $("#data-list-empresas").append("<option value =" + response.empresa[index].nome.replace(/\s/g,"�") + " data-id=" + response.empresa[index].id + ">" + response.empresa[index].id + "</option>" ); 
				});
				
				$("#container-perfil-usuario-informacoes #data-de-cadastro").text(response.dataCadastro);
				
				if (response.status == true) {
					$("#container-perfil-usuario-informacoes #status").text("Ativo");	
				}else{
					$("#container-perfil-usuario-informacoes #status").text("Desativado");
				}
				
				
			}).fail(function(response, textstatus, teste) {
				
			});
						
	}
	/*Traz os procedimentos mais acessados pelo analista, em ordem de quantidade de 
	acessos do maior para o menor */
	function carregarTabelaDeProcedimentos(){
			$('#tabela-procedimentos-mais-acessados-corpo').empty();
			$.ajax({
				url : localIP + "/baseDeConhecimento/relatorio/procedimento/" + localStorage.getItem("busca") +  "/analista",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers : {
				"Authorization" : localStorage.getItem("token")
				},
				}).done(function(response) {
					$.each(response, function(index, value){
						$.each(value, function(ind, val){
							
							tdHTML =  '<td>' + val.id + '</td>';
							tdHTML += '<td>' + val.titulo + '</td>';
							tdHTML += '<td>' + val.qtd_acesso + '</td>';
							
							trHTML += '<tr data-id=' + val.id + " onclick='buscarProcedimento(this)'>"  +  tdHTML +  '</tr>'; 
						});
						
						$("#tabela-procedimentos-mais-acessados-corpo").append(trHTML);
						$(trHTML).show('slow');
						
					});
					
					pesquisar() ;
			});
		}
	
	function pesquisar(){
		var $rows = $('#tabela-procedimentos-mais-acessados-corpo tr');
		$('#input-pesquisar-procedimento-mais-acessado').keyup(function() {
			
		  var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase().split(' ');

		  $rows.hide().filter(function() {
		    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
		    var matchesSearch = true;
		    $(val).each(function(index, value) {
		      matchesSearch = (!matchesSearch) ? false : ~text.indexOf(value);
		    });
		    return matchesSearch;
		  }).show();
		});
	}
	
	
	/* Traz os procedimentos mais acessados pelo analista na empresa escolhida
	ramb�m esta na ordem de quantidade de acessos do maior para o menor */
	$("#lista-empresas-do-usuario").on('input', function(){
		
		var value = $("#lista-empresas-do-usuario").val();
		var empresaID = $('#data-list-empresas [value="' + value + '"]').data('id')
		
		$("#tabela-procedimentos-mais-acessados-corpo").empty();
		
		console.log(empresaID)
		console.log(localStorage.getItem("usuario_id"))
	    $.ajax({
	    	url : localIP + "/baseDeConhecimento/relatorio/procedimento/empresa/" + empresaID + "/analista/" + localStorage.getItem("busca"),
			dataType : "json",
			method : "GET",
			contentType : "application/json",
			headers: {
				"Authorization" : localStorage.getItem("token"),}
			}).done(function(response) {
				console.log(response)
				$.each(response, function(index, value){
					$.each(value, function(ind, val){
						tdHTML =  '<td>' + val.id + '</td>';
						tdHTML += '<td>' + val.titulo + '</td>';
						tdHTML += '<td>' + val.qtd_acesso + '</td>';
						
						trHTML = '<tr data-id=' + val.id + " onclick='buscarProcedimento(this)'>"  +  tdHTML +  '</tr>';

						$("#tabela-procedimentos-mais-acessados-corpo").append(trHTML);
						$(trHTML).show('slow');
					});
					
					
					
				});
				
				pesquisar() ;
				
			});
	})

	</script>
</body>
</html>