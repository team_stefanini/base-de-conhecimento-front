<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/perfilempresa.css" />" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:import url="../templates/header.jsp"></c:import>
	<c:import url="../visualizar/perfil.jsp"></c:import>
	<c:import url="../visualizar/loader.jsp"></c:import>
	
	<div id="container-perfil-empresa">
		<div id="container-perfil-empresa-logo">
			<img alt="" id="logo-da-empresa" src="">
			<ul>
				<li id="nome-da-empresa"></li>
				<li id="data-de-cadastro"></li>
				<li id="quem-cadastrou"></li>
			</ul>
		</div>
		
		<div id="container-perfil-empresa-imagens">
			<div id="container-perfil-empresa-imagens-lista">
				<ul id="listaImagens">
				</ul>
			</div>
			
			<div id="imagemMaior">
				<input type="hidden" id="imagemID">
				<img id="imagemEscolhida" alt="" src="">
				
				<label id="btnAdicionarImagemFake" >Selecionar Arquivo</label>
				<label id="btnEnviar" >Confirmar</label>
				<input type="file" id="btnAdicionarImagem"  multiple="multiple"></input>
				<input type="button" id="btnExcluirImagem" disabled="disabled"></input>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
	/* AQUI O LOADER*/
	$(document).ajaxStart(function() {
		//only add progress bar if added yet.
		  if ($("#progress").length === 0) {
		    $("body").append($("<div><dt/><dd/></div>").attr("id", "progress"));
		    $("#progress").width((50 + Math.random() * 30) + "%");
		  }
		});

		$(document).ajaxComplete(function() {
		//End loading animation
		    $("#progress").width("101%").delay(200).fadeOut(400, function() {
		      $(this).remove();
		    });
		});
		
		/* AQUI O LOADER*/
		
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	
	var options = '';
	var imagemID = '';
	var imagemLI = '';
	var empresaID = localStorage.getItem("busca");
	
		
		/*Traz a imagem e informações da empresa*/
		$(document).ready(function(){
			/* var empresaID =  $('#empresa').val() */
			$.ajax({
				url: localIP + "/baseDeConhecimento/empresa/" + empresaID,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				$("#nome-da-empresa").text(response.nome)
				$("#data-de-cadastro").text(response.dataCadastro)
				$("#logo-da-empresa").attr('src' , localIP + "/baseDeConhecimento" + response.imagemLogo.caminho)
					
			}).fail(function(response, textstatus, teste){
				console.log("erro nas imagens da empresa");
			});
			
			carregarImagens()
			/*traz as imagens da empresa */
		
		});
		
		function carregarImagens(){
			$.ajax({
				url: localIP + "/baseDeConhecimento/empresa/" + empresaID + "/imagem"  ,
				dataType: "Json",
				method: "GET",
				contentType : "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				
				var fotos = response.length;
				var foto = parseFloat((100/fotos).toFixed(4))

				$("#container-perfil-empresa-imagens-lista ul").empty(); 	
				$.each(response, function(index){
					
					/* barra(foto) */
					
					$("#container-perfil-empresa-imagens-lista ul").append("<li value=" + response[index].id +">" + "<img class='pequeno' src=" + localIP + "/baseDeConhecimento" + response[index].caminho + ">" + "</li>")
					/* $("#imagens-empresa ul").append("<li>"+ "<img class='pequeno' src=http://192.168.2.245/baseDeConhecimento" + response[index].caminho + ">" + "</li>") */
				});
					
			}).fail(function(response, textstatus, teste){
				console.log("erro nas imagens da empresa");
			});
		}

		/*Carrega a imagem em um tamanho maior*/
		$("#listaImagens").on('click', 'li', function() {

		   	imagemLI = $(this);
		   	imagemID = $(this).val();
		   	$("#btnExcluirImagem").attr('disabled', false);
		    $.ajax({
		    	url :localIP + "/baseDeConhecimento/imagem/" + imagemID,
		    	dataType: "Json",
		    	method: "GET",
		    	contentType: "application/json",
		    	headers : {
					"Authorization" : localStorage.getItem("token")
					},
		    }).done(function(response){
		    	
		    	$("#imagemID").val(response.id);
		    	$("#imagemEscolhida").fadeIn(200);
		    	$("#imagemEscolhida").attr('src' , localIP + "/baseDeConhecimento"+response.caminho);
		    }).fail(function(respose, textstatus, teste){
		    	console.log(response)
		    	console.log(textstatus)
		    	console.log(teste)
		    });
		});
		
		
		/*Excluir imagem*/
		$("#btnExcluirImagem").click(function(){
			
			$.ajax({
		    	url : localIP + "/baseDeConhecimento/imagem/" + imagemID,
		    	dataType: "Json",
		    	method: "DELETE",
		    	contentType: "application/json",
		    	headers : {
					"Authorization" : localStorage.getItem("token")
					},
		    }).done(function(response){
		    	imagemLI.hide('slow');
		    	$("#imagemEscolhida").fadeOut(200);
		    }).fail(function(response, textstatus, teste){
		    	console.log(response)
		    	console.log(textstatus)
		    	console.log(teste)
		    });
		});
		
		$('#btnAdicionarImagemFake').on('click', function() {
			  $('#btnAdicionarImagem').trigger('click');
		});
		
		
		var form;
	    $('#btnAdicionarImagem').change(function (event) {
	        form = new FormData();
	        form.append('id:', 1);
	        console.log(event.target.files)
	        $(event.target.files).each(function() {
				form.append('file', $(this)[0]);
			});
	        $('#btnAdicionarImagemFake').css("display", "none");
	        $("#btnEnviar").css({"opacity": "1", "z-index": "5", "display": "block"})
	    });
	    
	    $('#btnEnviar').click(function () {
	        $.ajax({
	            url: localIP + "/baseDeConhecimento/empresa/" + empresaID + "/imagem", 
	            data: form,
	            processData: false,
	            contentType: false,
	            type: 'POST',
	            headers : {					
					"Authorization" : localStorage.getItem("token")
				},
	            success: function (data) {
	                console.log("deu certo")
	                carregarImagens();
	                $('#btnAdicionarImagemFake').css("display", "block");
	    	        $("#btnEnviar").css({"opacity": "0", "z-index": "-5", "display": "none"})
	            }
	        });
	    });
	</script>
</body>
</html>