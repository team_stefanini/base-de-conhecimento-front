<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/perfil.css" />" rel="stylesheet" />

<div id="total"></div>

<div id="perfil">
		<div id="usuario">
			<h2>Perfil</h2>
		</div>

		<div id="informacoes">
			<input type="text" name="" id="perfilnome" disabled="disabled">
			<input type="email" name="" id="perfilemail" disabled="disabled">
			<input type="text" name="" id="perfiltipo" disabled="disabled">
			<ul id="empresa">Empresa(s):
			</ul>

			<input type="text" name="" id="perguntaSeguranca" placeholder="Pegunta de seguran�a" disabled="disabled">
			<input type="text" name="" id="respostaSeguranca" placeholder="Resposta de seguran�a" disabled="disabled">
			
			<input type="password" name="" id="senha" placeholder="Senha atual">
			<p id="alterarMinhaSenha">Alterar minha senha</p>
			<button type="button" id="meAlterar"></button>

		</div>
		
		<button type="button" id="confirmar" disabled="disabled"><span id="btnTexto" >Confirmar</span></button>		
</div>


<div id="container-alterar-minha-senha">
	<div>
		<label>Senha atual</label>
		<input type="password"  id="container-alterar-minha-senha-input-antiga-senha"></input><img id="ver-senha-atual" alt="" title="ver senha antiga" src="../../resources/imagens/eye.png">
		<label>Nova Senha</label>
		<input type="password" id="container-alterar-minha-senha-input-nova-senha"></input><img id="ver-senha-nova" alt="" title="ver senha nova" src="../../resources/imagens/eye.png">
		
	</div>
	
	<input id="container-alterar-minha-senha-btn-confirmar-troca-senha" type="button" value="confirmar">
</div>

<script>

/* $(document).ready(function(){
	if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
		$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
	}else{
		if (localStorage.getItem("usuario_tipo") == null) {
			$(window.location.href = "../../index.jsp").fadeIn('500');
		}
	}
})
 */
 
$("#azul").click(function(){
	$("#btnTexto").text("Confirmar")
	$("#confirmar").css("box-shadow", "")
	
	$.ajax({
		url: localIP + "/baseDeConhecimento/usuario/" + localStorage.getItem("usuario_id"),
		dataType: "json",
		method: "GET",
		contentType: "application/json",
		headers : {
			"Authorization" : localStorage.getItem("token")
			},
	}).done(function(response, textstatus, teste){
		$("#respostaSeguranca").css({"background-color": '', "border": ''})
		$("#informacoes  input").removeClass('erro');
		
		$("#informacoes ul li").empty();
		$("#perfilnome").val(response.nome);
		$("#perfilemail").val(response.email);
		$("#perfiltipo").val(response.tipoUsuario);
		$("#perguntaSeguranca").val(response.perguntaSeguranca);
		$("#respostaSeguranca").val(response.respostaSeguranca);
		
		$.each(response.empresa, function(index){
			$("#informacoes ul").append("<li>" + response.empresa[index].nome + ". </li>" );
		});
		$("#perfil").css('display', 'block');
	}).fail(function(response, textstatus, teste){
		console.log("teste = " + teste.status);
		console.log("response = " + response);
		console.log("CODIGO = " + textstatus);
	});
	$("#container-alterar-minha-senha-input-antiga-senha").val(''), 
	$("#container-alterar-minha-senha-input-nova-senha").val('')
	$("#container-alterar-minha-senha-btn-confirmar-troca-senha").val("Confirmar");
	$("#container-alterar-minha-senha-btn-confirmar-troca-senha").css("box-shadow", "inset 0px 0px 5px #73bfcb");
	$("#total").css({"opacity": "1", "background-color": "rgba(0, 0, 0, .7)", "z-index": "99"});
	$("#perfil").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "100"});
});

$("#total").click(function(){
	$("#total").css({"opacity": "0", "background-color": "rgba(0, 0, 0, 0)", "z-index": "-10"});
	$("#perfil").css({"animation": "none", "z-index": "-10", 'display': 'none'});
	$("#container-alterar-minha-senha").css({'display': 'none'})
});

$("#meAlterar").click(function(){
	$('#senha').css("display", "flex");
	$("#perfilnome").attr('disabled', false);
	$("#perfilemail").attr('disabled', false);
	$("#perguntaSeguranca").attr('disabled', false);
	$("#respostaSeguranca").attr('disabled', false);
	$("#confirmar").attr('disabled', false);
})

/* Verificar se selects est�o vazios */
function verificarCampos(){
	$("#informacoes  input").each(function(){
		if ($(this).val() == ''){ 
			$(this).addClass("erro");
		}else{
			$(this).removeClass("erro");
		}	
	});
}

$("#confirmar").click(function(){
	verificarCampos();
	
	var usuarioObj = {
			"id" : localStorage.getItem("usuario_id"),
			"nome": $("#perfilnome").val(),
			"email": $("#perfilemail").val(),
			"tipoUsuario": $("#perfiltipo").val(),
			"perguntaSeguranca": $("#perguntaSeguranca").val(),
			"respostaSeguranca": $("#respostaSeguranca").val(),
			"senha": $("#senha").val(),
		}
	
	console.log("USUARIO JSON -------------------------------------------------")
	console.log(usuarioObj)
	
	if($("#respostaSeguranca").val() == ''){
		$("#respostaSeguranca").css({"background-color": "#FFCFCF", "border": "1px  solid #ff0000"})
	}else{
		$("#respostaSeguranca").css({"background-color": '', "border": ''})
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario",
			dataType: "json",
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify ( usuarioObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function( response, textstatus, teste){
			$("#senha").val('')
			$("#btnTexto").text("Alterado")
			$("#confirmar").css("box-shadow", "inset 0px -100px 5px #73bfcb")
			
		}).fail(function( response, textstatus, teste){
			console.log("RESPONSEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
			console.log(response)
		});
	}
	
});
	
	
	$("#container-alterar-minha-senha-btn-confirmar-troca-senha").click(function(){
		
		$("#container-alterar-minha-senha-input-antiga-senha").val();
		$("#container-alterar-minha-senha-input-nova-senha").val();
		
		var userObj = {
			"senhaAntiga" : $("#container-alterar-minha-senha-input-antiga-senha").val(), 
			"novaSenha" : $("#container-alterar-minha-senha-input-nova-senha").val()
		}
		
/* 		console.log(userObj); */
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/usuario/alterarsenha/",
			dataType: "json",
			method: "POST",
			contentType: "application/json",
			data: JSON.stringify ( userObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function( response, textstatus, teste){
			console.log(response)
			console.log(response.status)
			console.log(teste)
			console.log(textstatus)
			$("#container-alterar-minha-senha-btn-confirmar-troca-senha").val("Alterado")
			$("#container-alterar-minha-senha-btn-confirmar-troca-senha").css("box-shadow", "inset 0px -100px 5px #73bfcb")
		}).fail(function( response, textstatus, teste){
			console.log(response)
			console.log(teste)
			console.log(textstatus)
			console.log(response.status)
			$("#container-alterar-minha-senha-btn-confirmar-troca-senha").val("Erro")
			$("#container-alterar-minha-senha-btn-confirmar-troca-senha").css("box-shadow", "inset 0px -100px 5px #7F2626")
			
			setTimeout(function(){
				$("#container-alterar-minha-senha-btn-confirmar-troca-senha").val("Confirmar");
				$("#container-alterar-minha-senha-btn-confirmar-troca-senha").css("box-shadow", "inset 0px 0px 5px #73bfcb");
			}, 1000);
			
			$("#container-alterar-minha-senha-input-antiga-senha").val(''), 
			$("#container-alterar-minha-senha-input-nova-senha").val('')
			$("#container-alterar-minha-senha-input-antiga-senha").focus();
		});
	});
	 
	  $("#ver-senha-atual")
	  .mouseup(function() {
		  $("#container-alterar-minha-senha-input-antiga-senha").attr('type', 'password');
	  })
	  .mousedown(function() {
		  $("#container-alterar-minha-senha-input-antiga-senha").attr('type', 'text');
	  });
	  
	  $("#ver-senha-nova")
	  .mouseup(function() {
		  $("#container-alterar-minha-senha-input-nova-senha").attr('type', 'password');
	  })
	  .mousedown(function() {
		  $("#container-alterar-minha-senha-input-nova-senha").attr('type', 'text');
	  });
	  
	  $("#alterarMinhaSenha").click(function(){
		$("#perfil").css('display', 'none');
		$("#perfil").css({'animation': 'movimentoFechar .5s', 'animation-fill-mode': 'fowards'})
		  $("#container-alterar-minha-senha").css('display', 'block')
			setTimeout(function(){
				$("#container-alterar-minha-senha").css({ "animation": "movimento .2s", "animation-fill-mode": "forwards", "z-index": "100"})
			}, 100);
		});
</script>
	