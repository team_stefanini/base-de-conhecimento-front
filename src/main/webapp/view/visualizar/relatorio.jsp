<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css" href="<c:url value="/resources/css/relatorio.css" />" rel="stylesheet" />

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/Chart.bundle.js" />"></script>

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />
	<c:import url="../visualizar/perfil.jsp" />
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.js" />"></script>	

	<div id="relatorio-container">
	
		<div id="relatorio-container-top">
		
			<div class="relatorio-container-top-divs" id="relatorio-container-top-empresas">
				<h1></h1>
				<p>Empresas registradas</p>
			</div>
			<hr class="separador">
			<div class="relatorio-container-top-divs" id="relatorio-container-top-categorias">
				<h1></h1>
				<p>Categorias cadastradas</p>
			</div>
			<hr class="separador">
			<div class="relatorio-container-top-divs" id="relatorio-container-top-procedimentos">
				<h1></h1>
				<p>Procedimentos Ativos</p>
			</div>
			<hr class="separador">
			<div class="relatorio-container-top-divs" id="relatorio-container-top-usuarios">
				<h1></h1>
				<p>Usuarios</p>
			</div>
		</div>
		
		<div id="relatorio-container-bottom">
			<div id="relatorio-container-bottom-grafico">
				<div id="relatorio-container-bottom-grafico-de-linha-titulo">
				
					<h1>Empresas X Funcionários</h1>
				</div>
				<div id="relatorio-container-bottom-grafico-de-linha">
					<canvas id="grafico-de-linhas" ></canvas>
				</div>
				
				<div id="container-data">
				 Empresa <input type="text" list="empresas" id="empresa" placeholder="Empresa">
				  <datalist id="empresas">
				  	
				  </datalist>
				  
				  
					Data <select id ="relatorioMeses" >
							<option value="1">1 Mês</option>
							<option value="2">2 Meses</option>
							<option value="3">3 Meses</option>
							<option value="4">4 Meses</option>
							<option value="5">5 Meses</option>
							<option value="6">6 Meses</option>
						</select>
				</div>
			</div>
			
			<div id="relatorio-container-bottom-aside-grafico">
				<div class="pie-graficos-container">
					<h5>Procedimentos mais acessados</h5>
					<div class="pie-grafico">
						<canvas id="grafico-doughnut-1"></canvas>
					</div>
					<div class="legenda-grafico">
						<ul id="grafico-doughnut-1-legendas">
						</ul>
					</div>
				</div>
				<div class="pie-graficos-container">
					<h5>Tipos de usuario do sistema</h5>
					<div class="pie-grafico">
						<canvas id="grafico-doughnut-2"></canvas>
					</div>
					<div class="legenda-grafico">
					<ul>
						<li><span id="legenda-analista">Analista</span><span id="legenda-analista-porcentagem"></span></li>
						<li><span id="legenda-coordenador">Coordernadores</span><span id="legenda-coordenador-porcentagem"></span></li>
					</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<script type="text/javascript">
	
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	/*Lista a quantidade de usuarios em cada empresa dos sistema*/
	var empresaTotal = [];
	var funcionariosTotal = [];
	
		$(document).ready(function(){
			$.ajax({
				url: localIP + "/baseDeConhecimento/relatorio/empresa",
				dataType: "json",
				method: "GET",
				contentType : "application/json",
				headers : {
					"Authorization" : localStorage.getItem("token")
					},
			}).done(function(response, textstatus, teste){		
				
				$('#relatorio-container-top-empresas h1').text(response.qntEmpresas);
				
				$("#relatorio-container-top-categorias h1").text(response.qntCategoriasPai);
				
				$("#relatorio-container-top-procedimentos h1").text(response.qtdProcedimentosSistema);
				
				$('#relatorio-container-top-usuarios h1').text(response.qntUsuarios);
							
				$.each(response.qntUsuariosPorEmpresa, function(index){
					var nome = index
					console.log(nome)
					$.each(response.qntUsuariosPorEmpresa[nome], function(i){
						var total = response.qntUsuariosPorEmpresa[nome][i];
						funcionariosTotal.push(total)

					});
					
					empresaTotal.push(nome);
					console.log(JSON.stringify(empresaTotal));
				
				});
				
				var titulo = [];
				var quantidade = [];
				var total = 0;
				
				/*Grafico procedimentos mais acessados da Stefanini*/
				$.each(response.qtdProcedimentosAcessadosAnalista.QtdProcedimentoAnalista, function(index, value){
					
					titulo.push(value.titulo);
					quantidade.push(value.qtd_acesso);
					total += value.qtd_acesso;
				});
				
				carregarGraficoQuantidadeCordenadoresAnalistas(response.qntAnalistas, response.qntCoodenadores, response.qntUsuarios);
				
				carregarGraficoProcedimentosMaisAcessados(titulo, quantidade, total);
				
				carregarGraficoTotalDeFuncionarios();
				 
				 /* console.log(response.qntUsuariosPorEmpresa.Adidas[6]) */
			}).fail(function(response, textstatus, teste){
				console.log("erro")
			});

		});
		
		/*Traz os 10 procedimentos mais acessados em todo o sistema*/
		var procedimentosTitulo = [];
		var procedimentosQtdAcessos = [];
		var grafico = '';
		function carregarGraficoTotalDeFuncionarios(){
			console.log(empresaTotal)
			console.log(funcionariosTotal)
			var ctx = document.getElementById("grafico-de-linhas");
			
			grafico = new Chart(ctx, {
			    type: 'line',
			    data: {
			    	labels: empresaTotal,
			    	datasets: [{
			    		fill: false,
			            lineTension: 0,
			            backgroundColor: "rgba(75, 191, 203, .5)",
			            borderColor: "rgba(75, 191, 203, 1)",
			            borderCapStyle: 'butt',
			            borderWidth: 5,
			            borderDash: [],
			            borderDashOffset: 0.0,
			            borderJoinStyle: 'bevel',
			            pointBorderColor: "#696969",
			            pointBackgroundColor: "#73bfcb",
			            pointBorderWidth: 1,
			            pointHoverRadius: 10,
			            pointHoverBackgroundColor: "rgba(75,192,192,1)",
			            pointHoverBorderColor: "rgba(0,0,0,1)",
			            pointHoverBorderWidth: 2,
			            pointRadius: 5,
			            pointHitRadius: 10,
			    		label: "Total de funcionários",
			    		data: funcionariosTotal,
			    		steppedLine	: false,
			    	}
			      ]
			    },
			    options: {
			    	/* tooltipCaretSize: 0, */
			         legend: {
			            display: false
			         },
			         tooltips: {
			            enabled: true
			         },
			    }
			}); 
		}
		var graficoProcedimentos = '';
		function carregarGraficoProcedimentosMaisAcessados(titulos, quantidades, total){			
			$("#grafico-doughnut-1-legendas").empty();
			
			$.each(titulos, function(indexT, valueT){
				var acessosDesteProcedimento = Math.round((quantidades[indexT]/total)*100);
				
				$("#grafico-doughnut-1-legendas ").append("<li><span class='grafico-doughnut-1-legendas-cores'>" + valueT + "</span><span>" + acessosDesteProcedimento + "%</span></li>")
			});
			
			var ctx = document.getElementById("grafico-doughnut-1");
			 
			graficoProcedimentos = new Chart(ctx, {
				type: 'pie',
					data: {
						labels: titulos,
						    datasets: [
						        {
						            data: quantidades,
						            backgroundColor: [
												"#73BFCB",
												"#82D8E5",
												"#90F0FF",
												"#6E7D7F",
												"#74C0CC",
												"#243C40",
												"#fafafa",
												"#D8DE3F",
												"#DCFAFF",
												"#1F727F",

						            ],
						            hoverBackgroundColor: [
											"#73BFCB",
											"#DDFAFF",
											"#90F0FF",
											"#6E7D7F",
											"#74C0CC",
											"#243C40",
											"#fafafa",
											"#D8DE3F",
											"#DCFAFF",
											"#82D8E5",
						            ]
						        }]
					},
					options:{
						legend: {
						    display: false,
						}
					}
			});		
			
			
		}
		
		var  graficoUsuarios = '';
		function carregarGraficoQuantidadeCordenadoresAnalistas(qntAnalistas, qntCoodenadores, total){
			tiposDeUsuario = [];
			tiposDeUsuario.push(qntAnalistas)
			tiposDeUsuario.push(qntCoodenadores)
			
			var analistas = ((qntAnalistas/total)*100).toFixed(0);
			var coordenadores = ((qntCoodenadores/total)*100).toFixed(0);
			
			
			$("#legenda-analista-porcentagem").text(analistas + "%");
			$("#legenda-coordenador-porcentagem").text(coordenadores + "%")
			var ctx = document.getElementById("grafico-doughnut-2");
			
			graficoUsuarios = new Chart(ctx, {
				type: 'pie',
					data: {
						labels: ["Analistas","Coordenadores"],
						    datasets: [
						        {
						            data: tiposDeUsuario,
						            backgroundColor: [
										"#90F0FF",              
						                "#243C40",
						                
						            ],
						            hoverBackgroundColor: [
						                "#B1EFFF",
						                "#447178",
						            ]
						        }]
					},
					options:{
						 legend: {
					            display: false,
					            labels: {
					                fontColor: '#696969',
					                fontSize: 12,
					                position: 'bottom',
					                boxWidth:  	10,
					            }
					        },layout: {
					            padding: {
					                left: 0,
					                right: 0,
					                top: 10,
					                bottom: 0
					            }
					        }
					}
				
			});
			
			
			
		/* 	ctx.onclick = function (evt) {
			      var activePoints = myDoughnutChart.getElementsAtEvent(evt);
			      var chartData = activePoints[0]['_chart'].config.data;
			      var idx = activePoints[0]['_index'];

			      var label = chartData.labels[idx];
					if (label == 'Analistas') {
						console.log("analista")
					}else{
						console.log("coordenador")
					}
			      
			    }; */
		}
		
		//Popular o datalist com as empresas do banco
		var options = '';
		$(document).ready(function(){
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/empresa",
				dataType: "json",
				method: "GET",
				contentType : "application/json",
				headers : {
					"Authorization" : localStorage.getItem("token")
					}
			}).done(function(response){
				$("#empresas").empty();
				$.each(response, function(index){
					
					options =  '<option>' + response[index].nome + '</option>';
					
					$("#empresas").append("<option value =" + response[index].nome.replace(/\s/g," ") + " data-id=" + response[index].id + ">" + response[index].id + "</option>" );
					
				});
				
			}).fail(function(response, textstatus, teste){
				console.log(teste.status);
				console.log(response.status);
				console.log(textstatus);
			});
		});
		
		var empresaID = null;
		$("#container-data #empresa").on('input', function(){
			var value =  $('#container-data #empresa').val();
			
			empresaID = $('#empresas [value="' + value + '"]').data('id'); 
			
			$.ajax({
				url: localIP + "/baseDeConhecimento/relatorio/empresa/" + empresaID ,
				dataType: "Json",
				method: "GET",
				contentType: "application/json",
				headers :{
					"Authorization": localStorage.getItem("token")
				}
			}).done(function(response){
				console.log("certo")
				tiposDeUsuario = [];
				tiposDeUsuario.push(response.qntAnalistas)
				tiposDeUsuario.push(response.qntCoodenadores)
				
				var titulo = [];
				var quantidade = [];
				var total = [];
				
				/*Grafico procedimentos mais acessados da Stefanini*/
				$.each(response.qtdProcedimentosAcessadosAnalista.QtdProcedimentoAnalista, function(index, value){
					/* procedimentosTitulo.push(procedimento); */
					
					console.log(value.id + " - " + value.titulo + " - " + value.qtd_acesso )
					
					titulo.push(value.titulo);
					quantidade.push(value.qtd_acesso);
					total += value.qtd_acesso;
					
				});				

				graficoProcedimentos.destroy();
				graficoUsuarios.destroy();				
				
				carregarGraficoProcedimentosMaisAcessados(titulo, quantidade, total);
				carregarGraficoQuantidadeCordenadoresAnalistas(response.qntAnalistas, response.qntCoodenadores, response.qntUsuarios);
				
				$('#relatorio-container-top-categorias h1').text(response.qntCategoriasPai);
				$('#relatorio-container-top-procedimentos h1').text(response.qntProcedimentos);
				$('#relatorio-container-top-usuarios h1').text(response.qntUsuarios);
				
			}).fail(function(response, textstatus, teste){
				console.log("ERRO");
			});
			
		});
		
		$(document).ready(function(){	
			var data =  new Date();
			var dia = data.getDay();
			var mes = data.getMonth()+1;
			var ano = data.getFullYear();

			var hoje = dia + '/' + mes + '/' + ano 
			
			var meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio",
  				"Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
				
				ultimoMes = 5;
				console.log(mes)
				primeiroMes = ultimoMes - 6;
				console.log(primeiroMes)
				for (i= primeiroMes; i < ultimoMes-1; i++) {
					
  				console.log(meses[i]);
			}
		});
		 
		
		$("#relatorioMeses").on('change', function(){
			grafico.destroy();
			graficoProcedimentos.destroy();
			graficoUsuarios.destroy();
			 gerarRelatorioPorData();
		});
		
		function gerarRelatorioPorData(){
		
			var relatorioObj = '';
				
			if (empresaID == null) {
					
				relatorioObj = {
						 'data' : $("#relatorioMeses").val(),
						 
				 }
				
				console.log('-------------------------------------------')
				console.log('EMPRESA ID ' + empresaID)
				console.log('-------------------------------------------')
				/* carregarGraficoTotalDeFuncionarios(); */

			}else{
				var value =  $('#container-data #empresa').val();
				empresaID = $('#container-data #empresas [value="' + value + '"]').data('id');
				
				console.log('+++++++++++++++++++++++++++++++++++++++++++')
				console.log('EMPRESA ID ' + empresaID)
				console.log('+++++++++++++++++++++++++++++++++++++++++++')
				
				 relatorioObj = {
						 'data' : $("#relatorioMeses").val(),
				 		 'idEmpresa' : 	empresaID
				 }
				 console.log("relatorio " + value);	 
			}
			
			 console.log(relatorioObj)
			 
				$.ajax({
					url: localIP + "/baseDeConhecimento/relatorio/data",
					dataType: "Json",
					method: "POST",
					contentType: "application/json",
					data: JSON.stringify ( relatorioObj ),
					headers :{
						"Authorization": localStorage.getItem("token")
					},
				}).done(function(response){
					console.log(JSON.stringify(response))
					console.log("certo")
					tiposDeUsuario = [];
					tiposDeUsuario.push(response.qntAnalistas)
					tiposDeUsuario.push(response.qntCoodenadores)
					var totalUsuarios = response.qntAnalistas + response.qntCoodenadores;
/* 						
					if (typeof myDoughnutChart != 'undefined') {
						myDoughnutChart.destroy();
					}   */
					
					var titulo = [];
					var quantidade = [];
					var totalProcedimentos = [];
					
					
					/*Grafico procedimentos mais acessados */
					$.each(response.qtdProcedimentosAcessadosAnalista.QtdProcedimentoAnalista, function(index, value){
						/* procedimentosTitulo.push(procedimento); */
						
						titulo.push(value.titulo);
						quantidade.push(value.qtd_acesso);
						totalProcedimentos += value.qtd_acesso;
						
					});	
					carregarGraficoProcedimentosMaisAcessados(titulo, quantidade, totalProcedimentos);
					
					
					$.each(response.qntUsuariosPorEmpresa, function(index){
						var nome = index
						$.each(response.qntUsuariosPorEmpresa[nome], function(i){
							var total = response.qntUsuariosPorEmpresa[nome][i];
							funcionariosTotal.push(total)

						})
						
						empresaTotal.push(nome);
					
					});
					carregarGraficoQuantidadeCordenadoresAnalistas(response.qntAnalistas, response.qntCoodenadores, totalUsuarios);
					
					empresaTotal = [];
					funcionariosTotal = [];
					$.each(response.qntUsuariosPorEmpresa, function(index){
						var nome = index
						$.each(response.qntUsuariosPorEmpresa[nome], function(i){
							var total = response.qntUsuariosPorEmpresa[nome][i];
							funcionariosTotal.push(total)

						})
						
						empresaTotal.push(nome);
					
					});
					carregarGraficoTotalDeFuncionarios();
					
					$('#relatorio-container-top-categorias h1').text(response.qntCategoriasPai);
					$('#relatorio-container-top-procedimentos h1').text(response.qntProcedimentos);
					$('#relatorio-container-top-usuarios h1').text(response.qntUsuarios);
					
				}).fail(function(response, textstatus, teste){
					console.log(response.status)
					console.log(textstatus)
					console.log("ERRO");
				}); 
			
		}
/* 		 $('#btnGerarRelatorioPorData').on('click', function(){
			 
			 if (empresaID == '') {
					$("#container-data #empresa").addClass('erroEmpresa'); 
					setTimeout(function(){
						$("#container-data #empresa").removeClass('erroEmpresa');
					}, 700)
				}else{
					 var relatorioObj = {
							 'data' : $("#relatorioMeses").val(),
					 		 'idEmpresa' : 	empresaID
					 }
					 
					 console.log(relatorioObj)
						$.ajax({
							url: localIP + "/baseDeConhecimento/relatorio/data",
							dataType: "Json",
							method: "POST",
							contentType: "application/json",
							data: JSON.stringify ( relatorioObj ),
							headers :{
								"Authorization": localStorage.getItem("token")
							},
						}).done(function(response){
							console.log(JSON.stringify(response))
							console.log("certo")
							tiposDeUsuario = [];
							tiposDeUsuario.push(response.qntAnalistas)
							tiposDeUsuario.push(response.qntCoodenadores)
							var total = response.qntAnalistas + response.qntCoodenadores;
							
							if (typeof myDoughnutChart != 'undefined') {
								myDoughnutChart.destroy();
							}  
							carregarGraficoQuantidadeCordenadoresAnalistas(response.qntAnalistas, response.qntCoodenadores, total);
							$('#relatorio-container-top-categorias h1').text(response.qntCategoriasPai);
							$('#relatorio-container-top-procedimentos h1').text(response.qntProcedimentos);
							$('#relatorio-container-top-usuarios h1').text(response.qntUsuarios);
							
						}).fail(function(response, textstatus, teste){
							console.log(response.status)
							console.log(textstatus)
							console.log("ERRO");
						}); 
					}
				
			}); */
		
</script>
	
</body>
</html>