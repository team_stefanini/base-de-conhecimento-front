<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>

<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/categoria.css" />" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />

	<section id="container-cadastro-categoria" >
		<form action="">
			<div id="titulo">
				<h1>Categoria</h1>
			</div>
			<label>Empresa</label>
			<input list="empresas" id="empresa">
			<datalist id="empresas">
			
			</datalist>
			
			<label>Nome categoria</label>
			<input type="text" id="categoria">
			
			<button type="button" onclick="criarCategoria()"><span id="textobotao">Criar categoria</span> </button> 
		</form>
	</section>	

	<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})
	
	//Popular o datalist com as empresas do banco
	var options = '';
	
	$(document).ready(function(){
		
		console.log(localStorage.getItem("token"))
		$.ajax({
			url: localIP + "/baseDeConhecimento/empresa",
			dataType: "json",
			method: "GET",
			contentType : "application/json",
			headers : {
				"Authorization" : localStorage.getItem("token")
				}
		}).done(function(response){
			$.each(response, function(index){
				
				options =  '<option>' + response[index].nome + '</option>';
				
				$("#empresas").append("<option value = '" + response[index].id + "'>" + response[index].nome + "</option>" );
				
			});
			
		}).fail(function(response, textstatus, teste){
			console.log(teste.status);
			console.log(response.status);
			console.log(textstatus);
		});
	});
	
	$(document).ready(function(){
		buscarCategoria();
	});
	
	/* Criar Categoria */
	function buscarCategoria(){
		
		$.ajax({
			url: localIP + "/baseDeConhecimento/categoria/" + 1,
			dataType: "json",
			method: "GET",
			contentType: "application/json",
			//data: JSON.stringify ( categoriaObj ),
			headers : {
				"Authorization" : localStorage.getItem("token")
				},
		}).done(function(response, textstatus, teste){
			
			$("#empresa").val(response.empresa.nome)
			$("#categoria").val(response.nome)
			
			$("#textobotao").text("sucessso")
			$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})
		}).fail(function(response, textstatus, teste){
			$("#textobotao").text("Erro")
			$( "button" ).css( "background-color", "rgba(128, 0, 0, .7)" );
		});	
	}
	
	$("input[type='radio'][name='quantidadeEmpresa']").click(
            function(){
            	quantidadeEmpresa = $('input[name=quantidadeEmpresa]:checked', '#formulario-cadastro-usuario').val();
            	
            	console.log(quantidadeEmpresa);
            	
            	switch (quantidadeEmpresa) {
            	
            	
					case "1":
						console.log("BBB");
							$("#empresa1").css("display", "block");
						break;

					case "2":
							$("#empresa1").css("display", "block");
							$("#empresa2").css("display", "block");
						break;
						
					case "3":
						$("#empresa1").css("display", "block");
						$("#empresa2").css("display", "block");
						$("#empresa3").css("display", "block");
						break;
					}
            	
            }
        );
	

	
	</script>

</body>
</html>