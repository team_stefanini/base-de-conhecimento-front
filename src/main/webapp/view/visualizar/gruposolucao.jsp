<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />

<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link type="text/css" href="<c:url value="/resources/css/padrao.css" />" rel="stylesheet" />

<link type="text/css"
	href="<c:url value="/resources/css/gruposolucao.css" />" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<title>Stefanini</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<c:import url="../templates/header.jsp" />

	
	<section id="container-cadastro-gruposolucao" >
		<form id="formulario" enctype="multipart/form-data">
			<div id="titulo">
				<h1>Grupo de Solução</h1>
			</div>
			<label>Nome</label>
			<input type="text" id="nome" name="nome" autofocus="autofocus">
			
			<!-- <div id="image-holder"></div>  -->
			
			<button type="button"  onclick="criarGrupoSolucao()" ><span id="textobotao">Criar Grupo</span> </button> 
		</form>
	</section>	

	<script>
	$(document).ready(function(){
		if (localStorage.getItem("usuario_tipo") == "ANALISTA" ) {
			$(window.location.href = "../../view/analista/analista.jsp").fadeIn('500');
		}else{
			if (localStorage.getItem("usuario_tipo") == null) {
				$(window.location.href = "../../index.jsp").fadeIn('500');
			}
		}
	})

		$(document).ready(function(){
			buscarGrupoSolucao();
		});
		function buscarGrupoSolucao() {			

			$.ajax({
				url : localIP + "/baseDeConhecimento/grupo/" + 1,
				dataType : "json",
				method : 'GET',
				contentType : "application/json",
				headers : {					
					"Authorization" : localStorage.getItem("token")
				},
			}).done(function(response, textstatus, teste) {
				
				$("#nome").val(response.nome);
				
				$("#textobotao").text("sucessso")
				$("button").css({"box-shadow": "inset 0px -100px 5px #73bfcb", "border-top": "2px solid #fff"})

			}).fail(function(response, textstatus, teste) {
				console.log('STATUS ' + textstatus);
				console.log(response);
				
				$("#textobotao").text("Erro")
				$( "button" ).css({"box-shadow": "inset 0px -100px 5px  #802626", "border-top": "2px solid #fff" });

			});
		}
	</script>
</body>
</html>